<?php

namespace Database\Seeders;

use CfDigital\Delta\Core\Models\Setting;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Setting::create([
            'key' => 'test-setting',
            'template' => 'text',
            'to_layout' => 1,
            'value' => ''
        ]);
    }
}
