<?php

namespace Database\Seeders;

use CfDigital\Delta\Core\Models\Menu;
use CfDigital\Delta\Seo\Models\Structure;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Menu::create([
            'title' => 'Main menu',
            'max_depth' => 1,
            'comment' => 'Main menu',
        ])
            ->items()
            ->createMany([
                    [
                        'title' => 'One page',
                        'url' => '/one-page',
                        'type' => "internal_link",
                        'structure_id' => Structure::where('url', '/one-page')->first()->id ?? null,
                    ],
                    [
                        'title' => 'Two page',
                        'url' => '/two-pages',
                        'type' => "internal_link",
                        'structure_id' => Structure::where('url', '/two-pages')->first()->id ?? null,
                    ]
                ]
            );
    }
}
