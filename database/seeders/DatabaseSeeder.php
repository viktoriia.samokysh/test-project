<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\User;
use CfDigital\Delta\Core\database\seeders\CoreSeeder;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {

        $this->call([
            CoreSeeder::class
        ]);

        $role = Role::create(['name' => 'developer']);
        Permission::create(['name' => 'view drafts']);
        Permission::create(['name' => 'upload images']);
        Permission::create(['name' => 'view robots']);
        Permission::create(['name' => 'edit robots']);

        foreach ([
                     'role', 'user', 'page', 'infoblock', 'form',
                     'field', 'domain', 'structure', 'redirect', 'metatag'
                 ] as $role_base) {
            Permission::create(['name' => 'view ' . $role_base]);
            Permission::create(['name' => 'create ' . $role_base]);
            Permission::create(['name' => 'edit ' . $role_base]);
            Permission::create(['name' => 'delete ' . $role_base]);
        }
        $role->givePermissionTo(Permission::pluck('name')->toArray());

        $pass = 'testtest';
        $user = User::factory()->create(['email' => 'samokish.viktoria@gmail.com', 'password' => bcrypt($pass)]);
        $user->assignRole($role);

        echo $pass . PHP_EOL;
    }
}
