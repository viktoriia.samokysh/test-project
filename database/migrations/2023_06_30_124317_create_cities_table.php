<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('cities', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->json('title');
            $table->json('area');
            $table->json('region');
            $table->json('settlement_type');
            $table->bigInteger('area_id')->nullable();
            $table->string('np_sys_ref')->nullable();
            $table->string('delivery_sys_ref')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('cities');
    }
};
