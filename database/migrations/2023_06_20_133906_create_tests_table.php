<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tests', function (Blueprint $table) {
            $table->id();

            $table->json('text_translated')->nullable();
            $table->string('text')->nullable();
            $table->json('textarea_translated')->nullable();
            $table->string('textarea')->nullable();
            $table->boolean('boolean')->nullable();
            $table->integer('number')->nullable();
            $table->date('date')->nullable();
            $table->dateTime('datetime')->nullable();
            $table->json('editor_translated')->nullable();
            $table->json('editor')->nullable();
            $table->nestedSet();
            $table->string('status')->default(CfDigital\Delta\Core\Enums\PublishStatus::Published->value);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('tests', function (Blueprint $table) {
            $table->drop();
        });
    }
};
