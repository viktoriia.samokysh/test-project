<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('services', function (Blueprint $table) {
            $table->id();

            $table->json('name');
            $table->date('add_date');
            $table->json('description');
            $table->string('slug')->unique();
            $table->nestedSet();
            $table->string('status')->default(CfDigital\Delta\Core\Enums\PublishStatus::Published->value);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('services', function (Blueprint $table) {
            $table->drop();
        });
    }
};
