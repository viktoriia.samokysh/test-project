<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('banners', function (Blueprint $table) {
            $table->id();

            $table->json('title');
            $table->integer('images_number');
            $table->string('template')->nullable();
            $table->nestedSet();
            $table->string('status')->default(CfDigital\Delta\Core\Enums\PublishStatus::Published->value);
            $table->foreignIdFor(config('delta.models.domain'))->nullable()->constrained()->nullOnDelete();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('banners', function (Blueprint $table) {
            $table->drop();
        });
    }
};
