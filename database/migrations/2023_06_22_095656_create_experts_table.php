<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('experts', function (Blueprint $table) {
            $table->id();

            $table->json('full_name');
            $table->json('post');
            $table->integer('rating');
            $table->string('email');
            $table->string('facebook_link')->nullable();
            $table->json('quote');
            $table->string('slug')->unique();
            $table->nestedSet();
            $table->string('status')->default(CfDigital\Delta\Core\Enums\PublishStatus::Published->value);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('experts', function (Blueprint $table) {
            $table->drop();
        });
    }
};
