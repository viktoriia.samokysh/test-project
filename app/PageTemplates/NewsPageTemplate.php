<?php

namespace App\PageTemplates;

use App\Modules\Posts\Http\Resources\NewsInnerResource;
use App\Modules\Posts\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

class NewsPageTemplate extends PageTemplate
{
    public static string $name = 'news-list';

    public function getData(Request $request, ?Model $page = null)
    {
        $result = parent::getData($request, $page);

        $result['data']['news'] = NewsInnerResource::collection(Post::query()
            ->where('template', 'news')
            ->when($request->search, function($query) use ($request) {
                return $query->where('title', 'like', '%'. $request->search . '%')
                    ->orWhere('content', 'like', '%'. $request->search . '%');
            })
            ->defaultOrder()
            ->get());

        //$result['data']['news_url'] = noApiRoute('news');

        return $result;
    }
}
