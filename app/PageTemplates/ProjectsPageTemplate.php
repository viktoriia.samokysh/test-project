<?php

namespace App\PageTemplates;

use App\Modules\Projects\Http\Resources\ProjectsResource;
use App\Modules\Projects\Models\Project;
use CfDigital\Delta\Seo\Models\Structure;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

//потрібно добавити ресурс для виводу даних

class ProjectsPageTemplate extends PageTemplate
{
    public static string $name = 'projects'; //назва шаблона

    public function getData(Request $request, ?Model $page = null) //головний метод для виводу даних
    {
        $result = parent::getData($request, $page);

        //всі дані, які передаються на фронт, добавляємо в $result['data']
        $result['data']['projects'] = ProjectsResource::collection(Project::query()
            ->when($request->search, function($query) use ($request) {
                return $query->where('title', 'like', '%'. $request->search . '%')
                    ->orWhere('content', 'like', '%'. $request->search . '%');
            })
            ->defaultOrder()
            ->get());
        $result['data']['projects_url'] = noApiRoute('projects');

        return $result;
    }

    public function getGoogleJsonLdData()
    {
        $data = parent::getGoogleJsonLdData();

        $data['@type'] = 'CollectionPage';

        return $data;
    }

    public function variablesList(): array //для Seo
    {
        $model = $this->getModel();
        $withValues = false;

        if ($model instanceof Structure) {
            $withValues = (bool)$model->title;
        }

        return [
            'title' => [
                'value' => $withValues ? $model->title : null,
                'description' => trans('delta::meta.seo.title')
            ],
            'h1_title' => [
                'value' => $withValues ? ($model->h1_title ?: $model->title) : null,
                'description' => trans('delta::meta.seo.h1_title')
            ]
        ];
    }
}
