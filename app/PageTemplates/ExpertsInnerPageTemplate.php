<?php

namespace App\PageTemplates;

use App\Modules\Experts\Http\Resources\ExpertInnerResource;
use App\Modules\Experts\Models\Expert;
use CfDigital\Delta\Seo\Models\Structure;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class ExpertsInnerPageTemplate extends PageTemplate
{
    public static string $name = 'expertsInner';

    public function getData(Request $request, ?Model $page = null)
    {
        $result = parent::getData($request, $page);

        $model = $this->getModel();

        if ($model instanceof Structure) {
            $expert = Expert::whereHas('structures', fn ($query) => $query->where('id', $model->id))->first();
        } elseif ($model instanceof Expert) {
            $expert = $model;
        }

        $result['data']['expert'] = new ExpertInnerResource($expert);
        //$result['data']['expert_url'] = noApiRoute('team');

        return $result;
    }
}
