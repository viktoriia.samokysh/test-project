<?php

namespace App\PageTemplates;

//use App\Modules\Sliders\Models\Slider;
//use CfDigital\Delta\Core\Services\CRUDFormGenerator;
use App\Modules\Banners\Http\Resources\BannerResource;
use App\Modules\Banners\Http\Resources\HomeImageResource;
use App\Modules\Banners\Http\Resources\TopImageResource;
use App\Modules\Banners\Models\Banner;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class HomepageTemplate extends PageTemplate
{
    public static string $name = 'homepage';

    public function getData(Request $request, ?Model $page = null)
    {
        $page = $page ?: config('delta.models.domain')::where('name', getDomainName($request))->firstOrFail();

        $result = parent::getData($request, $page);

        $banner = Banner::where('template', 'home')
            ->where('status', 'published')
            ->first();

        $banner->imageTemplateClass = HomeImageResource::class;

        $result['data']['banner'] = new BannerResource($banner);

        return $result;
    }

    public static function getFields(Model $page): array
    {
        return [];
//        return CRUDFormGenerator::new($page)
//            ->tabless()
//            ->addSelectField('slider_id', [
//                'value' => $page->slider_id,
//                'data' => Slider::select(['title', 'id'])
//                    ->get()
//                    ->map(fn ($slider) => [
//                        'label' => $slider->title,
//                        'value' => $slider->id
//                    ])
//                    ->toArray()
//            ])
//            ->get();
    }

    public static function rules(): array
    {
        return [
//            'slider_id' => 'nullable|exists:sliders,id'
        ];
    }

    public function getGoogleJsonLdData()
    {
        $data = parent::getGoogleJsonLdData();

        $data['@type'] = 'Organization';
//        $data['logo'] = $this->structure->logo;

        return $data;
    }
}
