<?php

namespace App\PageTemplates;

use CfDigital\Delta\Core\Rules\ImageRule;
use CfDigital\Delta\Core\Services\CRUDFormGenerator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class AboutUsTemplate extends PageTemplate
{
    public static string $name = 'about-us';

    public function getData(Request $request, ?Model $page = null)
    {
        $page = $page ?: config('delta.models.domain')::where('name', getDomainName($request))->firstOrFail();

        return parent::getData($request, $page);
    }

    public static function getFields(Model $page): array
    {
        return CRUDFormGenerator::new($page)
            ->tabless()
            ->addColorField('color', ["default" => "000000", 'value'=> $page->extra[default_locale()]['color'] ?? ''])
            ->addTextField('slogan', ['value'=> $page->extra[default_locale()]['slogan'] ?? ''])
            ->addImageField('booklet',
                [
                    "quantity" => 1,
                    "accept" => "image/*",
                    "sizes" => [
                        "list" => [
                            "width" => 600
                        ]
                    ],
                ])
            ->get();
    }

    public function getMedia()
    {
        return  [
            'booklet' => [
                'quantity' => 'single',
                'accept' => 'image/*',
                'sizes' => [
                    'list' => ['width' => 600]
                ]
            ]
        ];
    }

    public static function withInData(Request $request): array
    {
        return [
            'color' => $request->input('color'),
            'slogan' => $request->input('slogan'),
            'booklet' => getMediaFromRequest($request, 'booklet')
        ];
    }

    public static function rules(): array
    {
        return [
            'color' => 'nullable',
            'slogan' => 'nullable',
            'booklet' => ['nullable', new ImageRule()],
        ];
    }

    public function getGoogleJsonLdData()
    {
        $data = parent::getGoogleJsonLdData();
        $data['@type'] = 'Organization';

        return $data;
    }
}
