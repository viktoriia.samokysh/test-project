<?php

namespace App\PageTemplates;

use App\Modules\Posts\Http\Resources\NewsInnerResource;
use App\Modules\Posts\Models\Post;
use CfDigital\Delta\Seo\Models\Structure;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class NewsTemplate extends PageTemplate
{
    public static string $name = 'news';

    public function getData(Request $request, ?Model $page = null)
    {
        $result = parent::getData($request, $page);

        $model = $this->getModel();

        if ($model instanceof Structure) {
            $post = Post::whereHas('structures', fn ($query) => $query->where('id', $model->id))->first();
        } elseif ($model instanceof Post) {
            $post = $model;
        }

        $result['data']['news'] = new NewsInnerResource($post);
        //$result['data']['news_url'] = noApiRoute('posts');

        return $result;
    }
}
