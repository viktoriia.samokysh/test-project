<?php

namespace App\PageTemplates;

use App\Modules\Posts\Http\Resources\PublicationsInnerResource;
use App\Modules\Posts\Models\Post;
use CfDigital\Delta\Seo\Models\Structure;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class PublicationsTemplate extends PageTemplate
{
    public static string $name = 'publications';

    public function getData(Request $request, ?Model $page = null)
    {
        $result = parent::getData($request, $page);

        $model = $this->getModel();

        if ($model instanceof Structure) {
            $post = Post::whereHas('structures', fn ($query) => $query->where('id', $model->id))->first();
        } elseif ($model instanceof Post) {
            $post = $model;
        }

        $result['data']['publications'] = new PublicationsInnerResource($post);
        //$result['data']['publications_url'] = noApiRoute('posts');

        return $result;
    }
}
