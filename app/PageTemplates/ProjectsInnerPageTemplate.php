<?php

namespace App\PageTemplates;

use App\Modules\Projects\Http\Resources\ProjectInnerResource;
use App\Modules\Projects\Http\Resources\ProjectsResource;
use App\Modules\Projects\Models\Project;
use CfDigital\Delta\Seo\Models\Structure;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class ProjectsInnerPageTemplate extends PageTemplate
{
    public static string $name = 'projectsInner';

    public function getData(Request $request, ?Model $page = null)
    {
        $result = parent::getData($request, $page);

        $model = $this->getModel();

        if ($model instanceof Structure) {
            $project = Project::whereHas('structures', fn ($query) => $query->where('id', $model->id))->first();
        } elseif ($model instanceof Project) {
            $project = $model;
        }
        $result['data']['project'] = new ProjectInnerResource($project);

        $result['data']['projects'] = ProjectsResource::collection(Project::query()
            ->where('id', '!=', $project->id)
            ->defaultOrder()
            ->limit(6)
            ->get());

        $result['data']['projects_url'] = noApiRoute('projects');

        return $result;
    }
}
