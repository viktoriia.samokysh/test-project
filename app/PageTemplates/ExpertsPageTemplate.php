<?php

namespace App\PageTemplates;

use App\Modules\Experts\Http\Resources\ExpertsResource;
use App\Modules\Experts\Models\Expert;
use CfDigital\Delta\Seo\Models\Structure;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class ExpertsPageTemplate extends PageTemplate
{
    public static string $name = 'experts';

    public function getData(Request $request, ?Model $page = null)
    {
        $result = parent::getData($request, $page);

        $result['data']['experts'] = ExpertsResource::collection(Expert::query()
            ->when($request->search, function($query) use ($request) {
                return $query->where('title', 'like', '%'. $request->search . '%');
            })
            ->defaultOrder()
            ->get());

        //$result['data']['experts_url'] = noApiRoute('team');

        return $result;
    }

    public function getGoogleJsonLdData()
    {
        $data = parent::getGoogleJsonLdData();

        $data['@type'] = 'CollectionPage';

        return $data;
    }
}
