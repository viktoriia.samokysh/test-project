<?php

namespace App\PageTemplates;

use App\Http\Resources\PagesResource;
use CfDigital\Delta\Core\Models\Page;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class PagesPageTemplate extends PageTemplate
{
    public static string $name = 'pages';

    public function getData(Request $request, ?Model $page = null)
    {
        $result = parent::getData($request, $page);

        $result['data']['pages'] = PagesResource::collection(Page::query()
            ->when($request->search, function($query) use ($request) {
                return $query->where('title', 'like', '%'. $request->search . '%')
                    ->orWhere('content', 'like', '%'. $request->search . '%');
            })
            ->defaultOrder()
            ->get());

        //$result['data']['pages_url'] = noApiRoute('pages');

        return $result;
    }
}
