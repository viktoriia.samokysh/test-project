<?php

namespace App\PageTemplates;

use App\Modules\Services\Http\Resources\ServiceInnerResource;
use App\Modules\Services\Models\Service;
use CfDigital\Delta\Seo\Models\Structure;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class ServicesInnerPageTemplate extends PageTemplate
{
    public static string $name = 'servicesInner';

    public function getData(Request $request, ?Model $page = null)
    {
        $result = parent::getData($request, $page);

        $model = $this->getModel();

        if ($model instanceof Structure) {
            $service = Service::whereHas('structures', fn ($query) => $query->where('id', $model->id))->first();
        } elseif ($model instanceof Service) {
            $service = $model;
        }

        $result['data']['service'] = new ServiceInnerResource($service);
        //$result['data']['services_url'] = noApiRoute('services');

        return $result;
    }
}
