<?php

namespace App\Modules\Banners\Models;

use App\BannerTemplates\BannerImageTemplate;
use App\BannerTemplates\BannerTemplate;
use CfDigital\Delta\Core\Enums\PublishStatus;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Modules\Banners\Database\Factories\BannerFactory;
use Spatie\LaravelData\WithData;
use Spatie\ModelStates\HasStates;
use Spatie\Translatable\HasTranslations;
use Kalnoy\Nestedset\NodeTrait;
use CfDigital\Delta\Core\Services\Traits\HasPublishStatus;
use CfDigital\Delta\Core\Services\Traits\HasDomain;

class Banner extends Model
{
    use HasTranslations;
    use NodeTrait;
    use HasPublishStatus;
    use HasDomain;
    use HasFactory;
    use WithData;
    use HasStates;

    protected $table = 'banners';
    public $fillable = ['title', 'images_number', 'template', 'status'];

    public $translatable = ['title'];

    protected static function newFactory()
    {
        return new BannerFactory();
    }

    public function images()
    {
        return $this->hasMany(BannerImage::class, 'banner_id', 'id');
    }

    public function imagesInRandomOrder()
    {
        return $this->hasMany(BannerImage::class)->inRandomOrder();
    }
}
