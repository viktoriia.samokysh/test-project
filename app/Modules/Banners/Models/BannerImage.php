<?php

namespace App\Modules\Banners\Models;

use App\BannerTemplates\BannerImageTemplate;
use App\Modules\Banners\Backend\Tables\BannersTable;
use CfDigital\Delta\Core\Enums\PublishStatus;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Modules\Banners\Database\Factories\BannerImagesFactory;
use Spatie\LaravelData\WithData;
use Kalnoy\Nestedset\NodeTrait;
use CfDigital\Delta\Core\Services\Contracts\MediaContract;
use CfDigital\Delta\Core\Services\Traits\HasMedia;
use Spatie\ModelStates\HasStates;
use Spatie\Translatable\HasTranslations;

class BannerImage extends Model implements MediaContract
{
    use HasTranslations;
    use NodeTrait;
    use HasMedia;
    use HasFactory;
    use WithData;
    use HasStates;

    protected $table = 'banner_images';
    public $fillable = ['image', 'mobile_image', 'link', 'extra', 'banner_id', 'template'];

    protected $casts = [
        'status' => PublishStatus::class,
        'extra' => 'array',
        'template' => BannerImageTemplate::class,
    ];

    public $mediaFields = [
        'image' => [
            'quantity' => 'single',
            'accept' => 'image/*',
            'sizes' => [
                'page' => ['width' => 600]
            ]
        ],
        'mobile_image' => [
            'quantity' => 'single',
            'accept' => 'image/*',
            'sizes' => [
                'page' => ['width' => 600]
            ]
        ]
    ];

    public $translatable = ['extra'];

    protected static function newFactory()
    {
        return new BannerImagesFactory();
    }

    public function banner()
    {
        return $this->belongsTo(Banner::class);
    }

    protected function getScopeAttributes()
    {
        return ['banner_id'];
    }

    public function getRouteKeyName()
    {
        return 'id';
    }
}
