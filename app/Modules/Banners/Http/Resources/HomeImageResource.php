<?php

namespace App\Modules\Banners\Http\Resources;

class HomeImageResource extends BannerImageResource
{
    public function toArray($request)
    {
        return array_merge(parent::toArray($request) , [
            'button_text' => $this->extra['button_text'] ?? null,
            'title' => $this->extra['title'] ?? null,
            'description' => $this->extra['description'] ?? null,
        ]);
    }
}
