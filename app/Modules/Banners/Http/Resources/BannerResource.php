<?php

namespace App\Modules\Banners\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BannerResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'title' => $this->title,
            'images_number' => $this->images_number,
            'template' => $this->template,
            'status' => $this->status,
            'images' => ($this->imageTemplateClass ?? BannerImageResource::class)::collection(
                $this->imagesInRandomOrder->take($this->images_number)
            ),
        ];
    }
}
