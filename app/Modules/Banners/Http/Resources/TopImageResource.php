<?php

namespace App\Modules\Banners\Http\Resources;

class TopImageResource extends BannerImageResource
{
    public function toArray($request)
    {
        return array_merge(parent::toArray($request) , [
            'background_color' => $this->extra['color'] ?? null,
            'pattern' => $this->preparedMedia('list')['pattern'] ?? null
        ]);
    }
}
