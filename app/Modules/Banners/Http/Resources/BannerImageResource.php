<?php

namespace App\Modules\Banners\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BannerImageResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'link' => $this->link,
            'image' => $this->preparedMedia('list')['image'] ?? null,
            'mobile_image' => $this->preparedMedia('list')['mobile_image'] ?? null,
        ];
    }
}
