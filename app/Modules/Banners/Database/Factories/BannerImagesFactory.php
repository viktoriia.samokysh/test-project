<?php

namespace App\Modules\Banners\Database\Factories;

use Carbon\Carbon;
use App\Modules\Banners\Models\BannerImage;
use Illuminate\Database\Eloquent\Factories\Factory;

class BannerImagesFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = BannerImage::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $data = [];

        foreach (config('delta.supportedLocales') as $key => $value) {
            $data['title'][$key] = $this->faker->sentence;
        }

        return $data;
    }
}
