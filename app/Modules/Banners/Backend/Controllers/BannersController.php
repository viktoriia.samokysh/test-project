<?php

namespace App\Modules\Banners\Backend\Controllers;


use App\Modules\Banners\Backend\Data\BannerData;
use App\Modules\Banners\Backend\Tables\BannersTable;
use App\Modules\Banners\Models\Banner;
use CfDigital\Delta\Core\Actions\CreateAction;
use CfDigital\Delta\Core\Actions\UpdateAction;
use CfDigital\Delta\Core\Http\Controllers\Api\CRUDController;

class BannersController extends CRUDController
{
    protected string $table_class = BannersTable::class;
    protected string $model_class = Banner::class;
    protected string $data_class = BannerData::class;

    public function store(BannerData $data)
    {
        (new CreateAction())->handle($data->toArray(), new Banner);

        return response()->success();
    }

    public function update(BannerData $data, $banner)
    {
        $banner = Banner::where('id', $banner)->firstOrFail();

        (new UpdateAction())->handle($data->toArray(), $banner);

        return response()->success();
    }
}
