<?php

namespace App\Modules\Banners\Backend\Controllers;

use App\Modules\Banners\Backend\Data\BannerImagesData;
use App\Modules\Banners\Backend\Tables\BannerImagesTable;
use App\Modules\Banners\Models\BannerImage;
use CfDigital\Delta\Core\Actions\CreateAction;
use CfDigital\Delta\Core\Actions\CreateWithExtraAction;
use CfDigital\Delta\Core\Actions\UpdateAction;
use CfDigital\Delta\Core\Actions\UpdateWithExtraAction;
use CfDigital\Delta\Core\Http\Controllers\Api\CRUDController;

class BannerImagesController extends CRUDController
{
    protected string $table_class = BannerImagesTable::class;
    protected string $model_class = BannerImage::class;
    protected string $data_class = BannerImagesData::class;

    public function store(BannerImagesData $data, $banner)
    {
        (new CreateWithExtraAction())->handle([... $data->all(), 'banner_id' => $banner], new BannerImage);

        return response()->success();
    }

    public function update(BannerImagesData $data, $banner, $image)
    {
        $image = BannerImage::where('id', $image)->firstOrFail();

        (new UpdateWithExtraAction())->handle([... $data->all(), 'banner_id' => $banner], $image);

        return response()->success();
    }
}
