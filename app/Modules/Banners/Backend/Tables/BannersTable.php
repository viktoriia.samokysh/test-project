<?php

namespace App\Modules\Banners\Backend\Tables;

use App\Modules\Banners\Models\Banner;
use CfDigital\Delta\Core\Services\Table;
use Illuminate\Http\Request;

class BannersTable extends Table
{
    protected const COLUMNS = [
        'title' => [
            'type' => 'link'
        ],
        '__actions' => [
            'edit', 'delete'
        ]
    ];

    protected bool $orderable = true;

    protected string $model = Banner::class;

    protected function map($item): array
    {
        return [
            'id' => $item->id,
            'title' => $this->link($item->title, ['url' => noApiRoute('banners.images.index', ['banner' => $item->id])]),
        ];
    }

    protected function getBasePath(): string
    {
        return noApiRoute('banners.index');
    }
}
