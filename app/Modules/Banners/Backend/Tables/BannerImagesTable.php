<?php

namespace App\Modules\Banners\Backend\Tables;

use App\Modules\Banners\Models\BannerImage;
use CfDigital\Delta\Core\Services\Table;
use Illuminate\Http\Request;

class BannerImagesTable extends Table
{
    protected const COLUMNS = [
        'image' => [
            'type' => 'image'
        ],
        'mobile_image' => [
            'type' => 'image'
        ],
        '__actions' => [
            'edit', 'delete'
        ]
    ];

    protected ?string $parent_key = 'banner';
    protected string $parent_table = BannersTable::class;

    protected bool $orderable = true;

    protected string $model = BannerImage::class;

    protected function map($item): array
    {
        return [
            'id' => $item->id,
            'image' => $item->getImageSrc('image', width: 320),
            'mobile_image' => $item->getImageSrc('mobile_image', width: 320),
        ];
    }

    protected function getBasePath(): string
    {
        return noApiRoute('banners.images.index', ['banner' => $this->request?->route()?->parameter('banner')]);
    }
}
