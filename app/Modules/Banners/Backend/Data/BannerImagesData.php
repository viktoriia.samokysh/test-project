<?php

namespace App\Modules\Banners\Backend\Data;

use App\BannerTemplates\BannerImageTemplate;
use App\Modules\Banners\Models\Banner;
use App\Modules\Banners\Models\BannerImage;
use CfDigital\Delta\Core\Services\CRUDFormGenerator;
use CfDigital\Delta\Core\Services\Data;
use Illuminate\Database\Eloquent\Model;
use CfDigital\Delta\Core\Rules\ImageRule;

class BannerImagesData extends Data
{
    public function __construct(
        public array|null $image,
        public array|null $mobile_image,
        public string|null $link,
        public string|null $template,
    )
    {
    }

    public static function rules(): array
    {
        return [
            "image" => ['required', new ImageRule()],
            "mobile_image" => ['required', new ImageRule()],
            "link" => 'nullable',
            "template" => 'nullable',
        ];
    }

    public static function getTemplate()
    {
        $bannerId = request()->route()?->parameter('banner');
        $banner = Banner::find($bannerId);
        return $banner?->template;
    }

    public function with(): array
    {
        $result = [];
        $templateName = $this->getTemplate();

        if ($templateName) {
            $template = BannerImageTemplate::resolveStateClass($templateName);

            if (method_exists((new ($template)(new BannerImage)), 'withInData')) {
                $result = $template::withInData(request());
            }
        }

        return $result;
    }

    public static function fieldsForCreation(Model $model): array
    {
        $template = self::getTemplate();
        $templateInstance = BannerImageTemplate::resolveStateClass($template);
        $fields = $templateInstance::getFields($model);

        $form = CRUDFormGenerator::new($model, 'Default');

        $form->addUrlField('link')
            ->addMediaFields()
            ->addHiddenField('template', ['value' => $template]);

        if (!empty($fields)) {
            $form->addTab(
                trans('delta::fields.additional_fields'),
                [
                    'fields' => $fields,
                ]
            );
        }

        return $form->get();
    }
}
