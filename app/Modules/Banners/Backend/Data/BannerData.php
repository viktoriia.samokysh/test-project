<?php

namespace App\Modules\Banners\Backend\Data;

use App\Modules\Banners\Models\BannerImage;
use CfDigital\Delta\Core\Services\CRUDFormGenerator;
use CfDigital\Delta\Core\Services\Data;
use Illuminate\Database\Eloquent\Model;
use CfDigital\Delta\Core\Rules\ImageRule;

class BannerData extends Data
{
    public function __construct(
        public array $title,
        public int $images_number,
        public string|null $template,
        public \CfDigital\Delta\Core\Enums\PublishStatus|null $status,
    )
    {
    }

    public static function rules(): array
    {
        return [
            "title.".default_locale() => 'required',
            "images_number" => 'required',
            "template" => 'nullable',
            'status' => 'required',
        ];
    }

    public static function fieldsForCreation(Model $model): array
    {
        $templates = BannerImage::getStatesFor('template');
        $trans_prefix = 'delta::fields.templates';

        return CRUDFormGenerator::new($model, 'Default')
            ->addTextField('title', ["required" => true])
            ->addNumberField('images_number', ["required" => true, 'default' => 1])
            //->addTemplateField()
            ->addSelectField('template', [
                'required' => true,
                'data' => $templates->reject(function (string $template) {
                    return stripos($template, 'inner') !== false;
                })->map(fn ($template) => [
                    'value' => $template,
                    'label' => trans($trans_prefix . '.' . $template)
                ])
            ])
            ->addStatusField()
            ->get();
    }
}
