<?php

namespace App\Modules\Tests\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Modules\Tests\Database\Factories\TestFactory;
use Spatie\LaravelData\WithData;
use Spatie\Translatable\HasTranslations;
use Kalnoy\Nestedset\NodeTrait;
use CfDigital\Delta\Core\Services\Contracts\MediaContract;
use CfDigital\Delta\Core\Services\Traits\HasMedia;
use CfDigital\Delta\Core\Services\Traits\HasPublishStatus;

class Test extends Model implements MediaContract
{
    use HasTranslations;
    use NodeTrait;
    use HasMedia;
    use HasPublishStatus;
    use HasFactory;
    use WithData;

    protected $table = 'tests';
    public $fillable = ['text_translated', 'text', 'media', 'textarea_translated', 'textarea', 'boolean', 'number', 'date', 'datetime', 'editor_translated', 'editor', 'status'];

    public $mediaFields = [ 'media' => [
            'quantity' => 'single',
            'accept' => 'image/*',
            'sizes' => [
                'page' => ['width' => 600]
            ]
        ]];
    public $translatable = ['text_translated', 'textarea_translated'];

    protected static function newFactory()
    {
        return new TestFactory();
    }
}
