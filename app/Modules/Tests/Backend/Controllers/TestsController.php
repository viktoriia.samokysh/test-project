<?php

namespace App\Modules\Tests\Backend\Controllers;


use App\Modules\Tests\Backend\Data\TestData;
use App\Modules\Tests\Backend\Tables\TestsTable;
use App\Modules\Tests\Models\Test;
use CfDigital\Delta\Core\Actions\CreateAction;
use CfDigital\Delta\Core\Actions\UpdateAction;
use CfDigital\Delta\Core\Http\Controllers\Api\CRUDController;

class TestsController extends CRUDController
{
    protected string $table_class = TestsTable::class;
    protected string $model_class = Test::class;
    protected string $data_class = TestData::class;

    public function store(TestData $data)
    {
        (new CreateAction())->handle($data->toArray(), new Test);

        return response()->success();
    }

    public function update(TestData $data, $test)
    {
        $test = Test::where('id', $test)->firstOrFail();

        (new UpdateAction())->handle($data->toArray(), $test);

        return response()->success();
    }
}
