<?php

namespace App\Modules\Tests\Backend\Tables;

use App\Modules\Tests\Models\Test;
use CfDigital\Delta\Core\Services\Table;
use Illuminate\Http\Request;

class TestsTable extends Table
{
    protected const COLUMNS = [
        'title' => [
            'type' => 'text'
        ],
        '__actions' => [
            'edit', 'delete'        ]
    ];

    protected bool $orderable = true;

    protected string $model = Test::class;

    protected function map($item): array
    {
        return [
            'id' => $item->id,
            'title' => $this->text($item->title),
        ];
    }

    protected function getBasePath(): string
    {
        return noApiRoute('tests.index');
    }
}
