<?php

namespace App\Modules\Tests\Backend\Data;

use CfDigital\Delta\Core\Services\CRUDFormGenerator;
use CfDigital\Delta\Core\Services\Data;
use Illuminate\Database\Eloquent\Model;
use CfDigital\Delta\Core\Rules\ImageRule;

class TestData extends Data
{
    public function __construct(
        public array $text_translated,
        public string $text,
        public array|null $media,
        public array $textarea_translated,
        public string $textarea,
        public bool|null $boolean,
        public int $number,
        public string $date,
        public string $datetime,
        public array $editor_translated,
        public array $editor,
        public \CfDigital\Delta\Core\Enums\PublishStatus|null $status,
    )
    {
    }

    public static function rules(): array
    {
        return [
            "text_translated.".default_locale() => 'required',
            "text" => 'nullable',
            "media" => ['nullable', new ImageRule()],
            "textarea_translated.".default_locale() => 'nullable',
            "textarea" => 'nullable',
            "boolean" => 'nullable',
            "number" => 'nullable',
            "date" => 'nullable',
            "datetime" => 'nullable',
            "editor_translated" => 'nullable',
            "editor" => 'nullable',
            'status' => 'required',
        ];
    }

    public static function fieldsForCreation(Model $model): array
    {
        return CRUDFormGenerator::new($model, 'Default')
            // не виводить value в translated поле
            ->addTextField('text_translated', ['required' => true, 'value' => 'Default title'])
            //  не перевіряє на min
            ->addTextField('text', ['hint' => trans('delta::fields.hints.text'), 'min' => 7, 'max' => 10, 'placeholder' => 'test@mail.com', 'type' => 'email'])
            ->addTextareaField('textarea_translated')
            ->addTextareaField('textarea', ['value' => 'Default textarea'])
            ->addCheckboxField('boolean')
            ->addNumberField('number')
            ->addDateField('date', ['default' => '2023-06-10', 'max' => '2023-06-28'])
            ->addDatetimeField('datetime')
            ->addEditorField('editor_translated', ['preset' => 'minimal'])
            ->addEditorField('editor', ['preset' => 'extra'])
            ->addStatusField()
            ->addMediaFields()
            // не відображається
            //->addColorField('color', ["default" => "000000"])
            ->get();
    }
}
