<?php

namespace App\Modules\Posts\Database\Factories;

use Carbon\Carbon;
use App\Modules\Posts\Models\Post;
use Illuminate\Database\Eloquent\Factories\Factory;

class PostFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Post::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $data = [];

        foreach (config('delta.supportedLocales') as $key => $value) {
            $data['title'][$key] = $this->faker->sentence;
        }

        return $data;
    }
}
