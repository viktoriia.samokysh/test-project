<?php

namespace App\Modules\Posts\Http\Resources;

use App\Modules\Experts\Http\Resources\ExpertInnerResource;
use App\Modules\Experts\Models\Expert;
use Illuminate\Http\Resources\Json\JsonResource;

class PublicationsInnerResource extends JsonResource
{
    public function toArray($request)
    {
        $expert = isset($this->extra['expert_id']) ? Expert::find($this->extra['expert_id']) : null;

        return [
            'title' => $this->title,
            'content' => $this->content,
            'publication_date' => $this->publication_date,
            'expert' => new ExpertInnerResource($expert),
            'tags' => $this->extra['tags'] ?? null,
        ];
    }
}
