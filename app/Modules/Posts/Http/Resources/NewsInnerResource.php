<?php

namespace App\Modules\Posts\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class NewsInnerResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'title' => $this->title,
            'content' => $this->content,
            'publication_date' => $this->publication_date,
            'in_main' => $this->extra['in_main'] ?? null,
            'description' => $this->extra['description'] ?? null,
        ];
    }
}
