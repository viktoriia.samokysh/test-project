<?php

namespace App\Modules\Posts\Backend\Controllers;

use App\Modules\Posts\Backend\Data\PostData;
use App\Modules\Posts\Backend\Tables\PostsTable;
use App\Modules\Posts\Models\Post;
use CfDigital\Delta\Core\Actions\CreateAction;
use CfDigital\Delta\Core\Actions\CreateWithExtraAction;
use CfDigital\Delta\Core\Actions\UpdateAction;
use CfDigital\Delta\Core\Actions\UpdateWithExtraAction;
use CfDigital\Delta\Core\Http\Controllers\Api\CRUDController;

class PostsController extends CRUDController
{
    protected string $table_class = PostsTable::class;
    protected string $model_class = Post::class;
    protected string $data_class = PostData::class;

    public function store(PostData $data)
    {
        (new CreateWithExtraAction())->handle($data->toArray(), new Post);

        return response()->success();
    }

    public function update(PostData $data, $post)
    {
        $post = Post::where('id', $post)->firstOrFail();

        (new UpdateWithExtraAction())->handle($data->toArray(), $post);

        return response()->success();
    }
}
