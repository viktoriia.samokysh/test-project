<?php

namespace App\Modules\Posts\Backend\Data;

use App\PostTemplates\PostTemplate;
use Carbon\Carbon;
use App\Modules\Posts\Models\Post;
use CfDigital\Delta\Core\Services\Data;
use Illuminate\Database\Eloquent\Model;
use CfDigital\Delta\Core\Rules\ImageRule;
use CfDigital\Delta\Core\Services\CRUDFormGenerator;
use Illuminate\Support\Facades\Log;

class PostData extends Data
{
    public function __construct(
        public array $title,
        public array $content,
        public string $publication_date,
        public array|null $desktop_image,
        public array|null $mobile_image,
        public string|null $template,
        public \CfDigital\Delta\Core\Enums\PublishStatus|null $status,
    )
    {
    }

    public static function rules(): array
    {
        $rules = [
            "title.".default_locale() => 'required',
            "content.".default_locale() => 'required',
            "publication_date" => 'required',
            "desktop_image" => ['nullable', new ImageRule()],
            "mobile_image" => ['nullable', new ImageRule()],
            'status' => 'required',
        ];

        /*
        if (request()?->get('template')) {
            $template = config('delta.otherClasses.news_template')::resolveStateClass(request()?->get('template'));

            if (method_exists((new ($template)(new Post)), 'rules')) {
                $rules = array_merge($rules, $template::rules());
            }
        }*/

        return $rules;
    }


    public function with(): array
    {
        $result = [];

        if (request()?->get('template')) {
            $template = PostTemplate::resolveStateClass(request()?->get('template'));

            if (method_exists((new ($template)(new Post)), 'withInData')) {
                $result = $template::withInData(request());
            }
        }

        return $result;
    }

    public static function fieldsForCreation(Model $model): array
    {
        return CRUDFormGenerator::new($model, 'Default')
            ->addTextField('title', ["required" => true])
            ->addTextareaField('content', ["required" => true])
            ->addDateField('publication_date', ["required" => true, "default" => Carbon::now()->toDateString()])
            ->addTextField('template')
            ->addStatusField()
            ->addMediaFields()
            ->addTemplateField()
            ->get();
    }
}
