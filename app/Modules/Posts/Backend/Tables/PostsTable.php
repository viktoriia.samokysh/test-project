<?php

namespace App\Modules\Posts\Backend\Tables;

use App\Modules\Posts\Models\Post;
use CfDigital\Delta\Core\Services\Table;
use Illuminate\Http\Request;

class PostsTable extends Table
{
    protected const COLUMNS = [
        'title' => [
            'type' => 'text'
        ],
        '__actions' => [
            'edit', 'delete', 'show'
        ]
    ];

    protected bool $orderable = true;

    protected string $model = Post::class;

    protected function map($item): array
    {
        return [
            'id' => $item->id,
            'title' => $this->text($item->title),
            '__show' => $item->generateUrl(),
        ];
    }

    protected function getBasePath(): string
    {
        return noApiRoute('posts.index');
    }
}
