<?php

namespace App\Modules\Posts\Models;

use App\PostTemplates\PostTemplate;
use CfDigital\Delta\Core\Enums\PublishStatus;
use CfDigital\Delta\Core\Services\Traits\HasDomain;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Modules\Posts\Database\Factories\PostFactory;
use Spatie\LaravelData\WithData;
use Spatie\ModelStates\HasStates;
use Spatie\Sitemap\Contracts\Sitemapable;
use Spatie\Translatable\HasTranslations;
use Kalnoy\Nestedset\NodeTrait;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use CfDigital\Delta\Core\Services\Contracts\MediaContract;
use CfDigital\Delta\Core\Services\Traits\HasMedia;
use CfDigital\Delta\Seo\Services\Contracts\GoesToStructureContract;
use CfDigital\Delta\Seo\Services\Traits\GoesToStructure;
use Illuminate\Support\Str;
use CfDigital\Delta\Core\Services\Traits\HasPublishStatus;

class Post extends Model implements GoesToStructureContract, MediaContract
{
    use HasTranslations;
    use NodeTrait;
    use HasSlug;
    use HasMedia;
    use GoesToStructure;
    use HasPublishStatus;
    use HasFactory;
    use WithData;
    use HasStates;
    //use HasDomain;

    protected $table = 'posts';
    public $fillable = ['title', 'content', 'publication_date', 'desktop_image', 'mobile_image', 'template', 'extra', 'status'];

    public $mediaFields = [ 'desktop_image' => [
            'quantity' => 'single',
            'accept' => 'image/*',
            'sizes' => [
                'page' => ['width' => 600]
            ]
        ],
        'mobile_image' => [
            'quantity' => 'single',
            'accept' => 'image/*',
            'sizes' => [
                'page' => ['width' => 600]
            ]
        ]];

    public $translatable = ['title', 'content', 'extra'];
    //protected string $structure_template = "postsInner";

    protected $casts = [
        'status' => PublishStatus::class,
        'extra' => 'array',
        'show_in_main' => 'boolean',
        'template' => PostTemplate::class
    ];

    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('slug')
            ->slugsShouldBeNoLongerThan(50)
            ->doNotGenerateSlugsOnUpdate()
            ->preventOverwrite();
    }

    public function getRouteKeyName(): string
    {
        return 'slug';
    }

    public function generateUrl(): string
    {
        return Str::start('posts/' . $this->slug, '/');
    }

    protected static function newFactory()
    {
        return new PostFactory();
    }
}
