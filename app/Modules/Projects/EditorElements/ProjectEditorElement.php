<?php

namespace App\Modules\Projects\EditorElements;

use App\Modules\Projects\Models\Project;
use App\Modules\Projects\Http\Resources\ProjectsResource;
use CfDigital\Delta\Core\Services\EditorElements\EditorElement;

class ProjectEditorElement extends EditorElement
{
    protected $key = 'projects';

    public function render($value): ?array 	//дані, що потрібні для виводу на сайт. В адмінці в редакторі відображатись не будуть. Всі дані обробляються ресурсом, тому його також потрібно додати. Всі ресурси мають бути в одному місці. Якщо дані з виводу для даного компонента відрізняються від інших ресурсів, варто створити новий і відповідно назвати
    {
        return [
            'data' => [
                'projects' => ProjectsResource::collection(Project::query()
                    ->where('show_in_main', 1)	//умова відбору
                    ->defaultOrder()
                    ->get(['id', 'title', 'description', 'content', 'show_in_main', 'slug'])),
                'url' => noApiRoute('projects'),
            ],
            'type' => $this->key
        ];
    }

    public function adminData(): array //дані, що відображаються в редакторі.
    {
        return [
            'title' => 'Проекти',
            'template' => 'projects',
            'image' => '/images/components/projects.jpg',
            'comment' => 'Вивід проектів з чекбоксом на головну',
        ];
    }
}
