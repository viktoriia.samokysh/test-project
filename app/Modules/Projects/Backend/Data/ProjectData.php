<?php

namespace App\Modules\Projects\Backend\Data;

use CfDigital\Delta\Core\Services\CRUDFormGenerator;
use CfDigital\Delta\Core\Services\Data;
use Illuminate\Database\Eloquent\Model;
use CfDigital\Delta\Core\Rules\ImageRule;

class ProjectData extends Data
{
    public function __construct(
        public array $title,
        public array $description,
        public array $content,
        public bool|null $show_in_main,
        public array|null $desktop_image,
        public array|null $mobile_image,
        public \CfDigital\Delta\Core\Enums\PublishStatus|null $status,
    )
    {
    }

    public static function rules(): array
    {
        return [
            "title.".default_locale() => 'required',
            "description.".default_locale() => 'required',
            "content.".default_locale() => 'required',
            "show_in_main" => 'nullable',
            "desktop_image" => ['nullable', new ImageRule()],
            "mobile_image" => ['nullable', new ImageRule()],
            'status' => 'required',
        ];
    }

    public static function fieldsForCreation(Model $model): array
    {
        return CRUDFormGenerator::new($model, 'Default')
            ->addTextField('title', ["required" => true])
            ->addTextareaField('description', ["required" => true])
            ->addEditorField('content', ["required" => true])
            ->addCheckboxField('show_in_main', ['default' => true])
            ->addStatusField()
            ->addMediaFields()
            ->get();
    }
}
