<?php

namespace App\Modules\Projects\Backend\Tables;

use App\Modules\Projects\Models\Project;
use CfDigital\Delta\Core\Services\Table;
use Illuminate\Http\Request;

class ProjectsTable extends Table
{
    protected const COLUMNS = [
        'title' => [
            'type' => 'text'
        ],
        '__actions' => [
            'edit', 'delete', 'show'
        ]
    ];

    protected bool $orderable = true;

    protected string $model = Project::class;

    protected function map($item): array
    {
        return [
            'id' => $item->id,
            'title' => $this->text($item->title),
            '__show' => $item->generateUrl(),
        ];
    }

    protected function getBasePath(): string
    {
        return noApiRoute('projects.index');
    }
}
