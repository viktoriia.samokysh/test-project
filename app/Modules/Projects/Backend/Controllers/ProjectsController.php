<?php

namespace App\Modules\Projects\Backend\Controllers;


use App\Modules\Projects\Backend\Data\ProjectData;
use App\Modules\Projects\Backend\Tables\ProjectsTable;
use App\Modules\Projects\Models\Project;
use CfDigital\Delta\Core\Actions\CreateAction;
use CfDigital\Delta\Core\Actions\UpdateAction;
use CfDigital\Delta\Core\Http\Controllers\Api\CRUDController;

class ProjectsController extends CRUDController
{
    protected string $table_class = ProjectsTable::class;
    protected string $model_class = Project::class;
    protected string $data_class = ProjectData::class;

    public function store(ProjectData $data)
    {
        (new CreateAction())->handle($data->toArray(), new Project);

        return response()->success();
    }

    public function update(ProjectData $data, $project)
    {
        $project = Project::where('id', $project)->firstOrFail();

        (new UpdateAction())->handle($data->toArray(), $project);

        return response()->success();
    }
}
