<?php

namespace App\Modules\Projects\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Modules\Projects\Database\Factories\ProjectFactory;
use Spatie\LaravelData\WithData;
use Spatie\Translatable\HasTranslations;
use Kalnoy\Nestedset\NodeTrait;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use CfDigital\Delta\Core\Services\Contracts\MediaContract;
use CfDigital\Delta\Core\Services\Traits\HasMedia;
use CfDigital\Delta\Seo\Services\Contracts\GoesToStructureContract;
use CfDigital\Delta\Seo\Services\Traits\GoesToStructure;
use Illuminate\Support\Str;
use CfDigital\Delta\Core\Services\Traits\HasPublishStatus;

class Project extends Model implements GoesToStructureContract, MediaContract
{
    use HasTranslations;
    use NodeTrait;
    use HasSlug;
    use HasMedia;
    use GoesToStructure;
    use HasPublishStatus;
    use HasFactory;
    use WithData;

    protected $table = 'projects';
    public $fillable = ['title', 'description', 'content', 'show_in_main', 'desktop_image', 'mobile_image', 'status'];

    public $mediaFields = [ 'desktop_image' => [
            'quantity' => 'single',
            'accept' => 'image/*',
            'sizes' => [
                'page' => ['width' => 600]
            ]
        ],
        'mobile_image' => [
            'quantity' => 'single',
            'accept' => 'image/*',
            'sizes' => [
                'page' => ['width' => 600]
            ]
        ]];

    public $translatable = ['title', 'description', 'content'];
    protected string $structure_template = "projectsInner";

    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('slug')
            ->slugsShouldBeNoLongerThan(50)
            ->doNotGenerateSlugsOnUpdate()
            ->preventOverwrite();
    }

    public function getRouteKeyName(): string
    {
        return 'slug';
    }
    public function generateUrl(): string
    {
        return Str::start('projects/' . $this->slug, '/');
    }

    protected static function newFactory()
    {
        return new ProjectFactory();
    }
}
