<?php

namespace App\Modules\Projects\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use CfDigital\Delta\Seo\Models\Structure;
use Illuminate\Http\Request;
use function noApiRoute;

class ProjectController extends Controller
{
    public function __invoke(Request $request)
    {
        return Structure::query()
            ->withMeta()
            ->where('url', noApiRoute('projects'))
            ->firstOrFail()
            ->template
            ->getData($request);
    }
}
