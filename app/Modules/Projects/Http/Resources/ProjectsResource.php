<?php

namespace App\Modules\Projects\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ProjectsResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'title' => $this->title,
            'description' => $this->description,
            'content' => $this->content,
            'show_in_main' => $this->show_in_main,
            'desktop_image' => $this->preparedMedia('list')['desktop_image'] ?? null,
            'mobile_image' => $this->preparedMedia('list')['mobile_image'] ?? null,
        ];
    }
}

