<?php

namespace App\Modules\Services\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ServicesResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'name' => $this->title,
            'description' => $this->description,
            'add_date' => $this->add_date,
        ];
    }
}
