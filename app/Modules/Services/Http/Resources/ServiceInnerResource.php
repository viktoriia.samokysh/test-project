<?php

namespace App\Modules\Services\Http\Resources;

use App\Modules\Experts\Http\Resources\ExpertsResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ServiceInnerResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'name' => $this->title,
            'description' => $this->description,
            'add_date' => $this->add_date,
            'experts' => ExpertsResource::collection($this->experts),
        ];
    }
}
