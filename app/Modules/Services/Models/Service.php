<?php

namespace App\Modules\Services\Models;

use App\Modules\Experts\Models\Expert;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Modules\Services\Database\Factories\ServiceFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Spatie\LaravelData\WithData;
use Spatie\Translatable\HasTranslations;
use Kalnoy\Nestedset\NodeTrait;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use CfDigital\Delta\Core\Services\Contracts\MediaContract;
use CfDigital\Delta\Core\Services\Traits\HasMedia;
use CfDigital\Delta\Seo\Services\Contracts\GoesToStructureContract;
use CfDigital\Delta\Seo\Services\Traits\GoesToStructure;
use Illuminate\Support\Str;
use CfDigital\Delta\Core\Services\Traits\HasPublishStatus;

class Service extends Model implements GoesToStructureContract, MediaContract
{
    use HasTranslations;
    use NodeTrait;
    use HasSlug;
    use HasMedia;
    use GoesToStructure;
    use HasPublishStatus;
    use HasFactory;
    use WithData;

    protected $table = 'services';
    public $fillable = ['title', 'add_date', 'description', 'status'];

    public $mediaFields = [];
    public $translatable = ['title', 'description'];
    protected string $structure_template = "servicesInner";
    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('slug')
            ->slugsShouldBeNoLongerThan(50)
            ->doNotGenerateSlugsOnUpdate()
            ->preventOverwrite();
    }

    public function experts(): BelongsToMany
    {
        return $this->belongsToMany(Expert::class);
    }

    public function getRouteKeyName(): string
    {
        return 'slug';
    }
    public function generateUrl(): string
    {
        return Str::start('services/' . $this->slug, '/');
    }
    protected static function newFactory()
    {
        return new ServiceFactory();
    }
}
