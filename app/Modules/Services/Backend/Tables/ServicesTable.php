<?php

namespace App\Modules\Services\Backend\Tables;

use App\Modules\Services\Models\Service;
use CfDigital\Delta\Core\Services\Table;
use Illuminate\Http\Request;

class ServicesTable extends Table
{
    protected const COLUMNS = [
        'name' => [
            'type' => 'text'
        ],
        'add_date' => [
            'type' => 'text'
        ],
        'experts' => [
            'type' => 'text'
        ],
        '__actions' => [
            'edit', 'delete', 'show'
        ]
    ];

    protected bool $orderable = true;

    protected string $model = Service::class;

    protected function map($item): array
    {
        return [
            'id' => $item->id,
            'name' => $this->text($item->title),
            'add_date' => $this->text($item->add_date),
            'experts' => $this->text($item->experts->pluck('title')),
            '__show' => $item->generateUrl(),
        ];
    }

    protected function getBasePath(): string
    {
        return noApiRoute('services.index');
    }
}
