<?php

namespace App\Modules\Services\Backend\Controllers;


use App\Modules\Services\Backend\Data\ServiceData;
use App\Modules\Services\Backend\Tables\ServicesTable;
use App\Modules\Services\Models\Service;
use CfDigital\Delta\Core\Actions\CreateAction;
use CfDigital\Delta\Core\Actions\UpdateAction;
use CfDigital\Delta\Core\Http\Controllers\Api\CRUDController;

class ServicesController extends CRUDController
{
    protected string $table_class = ServicesTable::class;
    protected string $model_class = Service::class;
    protected string $data_class = ServiceData::class;

    public function store(ServiceData $data)
    {
        $service = (new CreateAction())->handle($data->toArray(), new Service);
        $service->experts()->sync($data->toArray()['expert_id'] ?? []);

        return response()->success();
    }

    public function update(ServiceData $data, $service)
    {
        $service = Service::where('id', $service)->firstOrFail();

        (new UpdateAction())->handle($data->toArray(), $service);
        $service->experts()->sync($data->toArray()['expert_id'] ?? []);

        return response()->success();
    }
}
