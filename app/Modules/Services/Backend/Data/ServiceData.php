<?php

namespace App\Modules\Services\Backend\Data;

use App\Modules\Experts\Models\Expert;
use Carbon\Carbon;
use CfDigital\Delta\Core\Services\CRUDFormGenerator;
use CfDigital\Delta\Core\Services\Data;
use Illuminate\Database\Eloquent\Model;
use CfDigital\Delta\Core\Rules\ImageRule;

class ServiceData extends Data
{
    public function __construct(
        public array $title,
        public string $add_date,
        public array $description,
        public array|null $expert_id,
        public \CfDigital\Delta\Core\Enums\PublishStatus|null $status,
    )
    {
    }

    public static function rules(): array
    {
        return [
            "title.".default_locale() => 'required',
            "add_date" => 'required',
            "description.".default_locale() => 'required',
            'status' => 'required',
            'expert_id' => 'nullable',
        ];
    }

    public static function fieldsForCreation(Model $model): array
    {
        return CRUDFormGenerator::new($model, 'Default')
            ->addTextField('title', ["required" => true])
            ->addDateField('add_date', ["required" => true, 'default' => Carbon::now()->toDateString(), 'max' => Carbon::now()->toDateString()])
            ->addEditorField('description', ["required" => true, 'preset' => 'minimal'])
            ->addSelectField('expert_id', [
                'options' => Expert::all()
                    ->map(fn ($expert) => [
                        'value' => $expert->id,
                        'label' => $expert->title,
                    ]),
                'multi' => true,
                'value' => $model->experts()->pluck('id')->toArray()
            ])
            ->addStatusField()
            ->get();
    }
}
