<?php

namespace App\Modules\Reviews\Backend\Data;

use CfDigital\Delta\Core\Services\CRUDFormGenerator;
use CfDigital\Delta\Core\Services\Data;
use Illuminate\Database\Eloquent\Model;
use CfDigital\Delta\Core\Rules\ImageRule;

class ReviewData extends Data
{
    public function __construct(
        public array $title,
        public array $post,
        public array $comment,
        public array|null $image,
        public \CfDigital\Delta\Core\Enums\PublishStatus|null $status,
        public string|null $date,
    )
    {
    }

    public static function rules(): array
    {
        return [
            "title.".default_locale() => 'required',
            "post.".default_locale() => 'required',
            "comment.".default_locale() => 'required',
            "image" => ['nullable', new ImageRule()],
            'status' => 'required',
            "date" => 'nullable',
        ];
    }

    public static function fieldsForCreation(Model $model): array
    {
        return CRUDFormGenerator::new($model, 'Default')
            ->addTextField('title', ["required" => true])
            ->addTextField('post', ["required" => true])
            ->addTextareaField('comment', ["required" => true])
            ->addStatusField()
            ->addMediaFields()
            ->addDateField('date')
            ->get();
    }
}
