<?php

namespace App\Modules\Reviews\Backend\Tables;

use App\Modules\Reviews\Models\Review;
use Carbon\Carbon;
use CfDigital\Delta\Core\Services\Table;
use Illuminate\Http\Request;

class ReviewsTable extends Table
{
    protected const COLUMNS = [
        'title' => [
            'type' => 'text'
        ],
        'post' => [
            'type' => 'text'
        ],
        'image' => [
           'type' => 'image'
        ],
        'date' => [
            'type' => 'text'
        ],
        '__actions' => [
            'edit', 'delete'
        ],
    ];

    protected bool $orderable = true;

    protected string $model = Review::class;

    protected function map($item): array
    {
        return [
            'id' => $item->id,
            'title' => $this->text($item->title),
            'post' => $this->text($item->post),
            'image' => $item->getImageSrc('image', width: 320),
            'date' => $this->text($item->date),
            '__delete' => Carbon::now()->toDateString() >= $item->date,
        ];
    }

    protected function getBasePath(): string
    {
        return noApiRoute('reviews.index');
    }
}
