<?php

namespace App\Modules\Reviews\Backend\Controllers;


use App\Modules\Reviews\Backend\Data\ReviewData;
use App\Modules\Reviews\Backend\Tables\ReviewsTable;
use App\Modules\Reviews\Models\Review;
use CfDigital\Delta\Core\Actions\CreateAction;
use CfDigital\Delta\Core\Actions\UpdateAction;
use CfDigital\Delta\Core\Http\Controllers\Api\CRUDController;
use Illuminate\Http\Request;

class ReviewsController extends CRUDController
{
    protected string $table_class = ReviewsTable::class;
    protected string $model_class = Review::class;
    protected string $data_class = ReviewData::class;

    public function store(ReviewData $data)
    {
        (new CreateAction())->handle($data->toArray(), new Review);

        return response()->success();
    }

    public function update(ReviewData $data, $review)
    {
        $review = Review::where('id', $review)->firstOrFail();

        (new UpdateAction())->handle($data->toArray(), $review);

        return response()->success();
    }
}
