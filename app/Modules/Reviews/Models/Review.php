<?php

namespace App\Modules\Reviews\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Modules\Reviews\Database\Factories\ReviewFactory;
use Spatie\LaravelData\WithData;
use Spatie\Translatable\HasTranslations;
use Kalnoy\Nestedset\NodeTrait;
use CfDigital\Delta\Core\Services\Contracts\MediaContract;
use CfDigital\Delta\Core\Services\Traits\HasMedia;
use CfDigital\Delta\Core\Services\Traits\HasPublishStatus;

class Review extends Model implements MediaContract
{
    use HasTranslations;
    use NodeTrait;
    use HasMedia;
    use HasPublishStatus;
    use HasFactory;
    use WithData;

    protected $table = 'reviews';
    public $fillable = ['title', 'post', 'comment', 'image', 'status', 'date'];

    public $mediaFields = [ 'image' => [
            'quantity' => 3,
            'accept' => 'image/*',
            'sizes' => [
                'page' => ['width' => 600]
            ]
        ]];
    public $translatable = ['title', 'post', 'comment'];

    protected static function newFactory()
    {
        return new ReviewFactory();
    }
}
