<?php

namespace App\Modules\Clients\Backend\Controllers;


use App\Modules\Clients\Backend\Data\ClientData;
use App\Modules\Clients\Backend\Tables\ClientsTable;
use App\Modules\Clients\Models\Client;
use CfDigital\Delta\Core\Actions\CreateAction;
use CfDigital\Delta\Core\Actions\UpdateAction;
use CfDigital\Delta\Core\Http\Controllers\Api\CRUDController;

class ClientsController extends CRUDController
{
    protected string $table_class = ClientsTable::class;
    protected string $model_class = Client::class;
    protected string $data_class = ClientData::class;

    public function store(ClientData $data)
    {
        (new CreateAction())->handle($data->toArray(), new Client);

        return response()->success();
    }

    public function update(ClientData $data, $client)
    {
        $client = Client::where('id', $client)->firstOrFail();

        (new UpdateAction())->handle($data->toArray(), $client);

        return response()->success();
    }
}
