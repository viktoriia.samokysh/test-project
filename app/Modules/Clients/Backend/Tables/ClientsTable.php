<?php

namespace App\Modules\Clients\Backend\Tables;

use App\Modules\Clients\Models\Client;
use CfDigital\Delta\Core\Services\Table;
use Illuminate\Http\Request;

class ClientsTable extends Table
{
    protected const COLUMNS = [
        'title' => [
            'type' => 'text'
        ],
        '__actions' => [
            'edit', 'delete'        ]
    ];

    protected bool $orderable = true;

    protected string $model = Client::class;

    protected function map($item): array
    {
        return [
            'id' => $item->id,
            'title' => $this->text($item->title),
        ];
    }

    protected function getBasePath(): string
    {
        return noApiRoute('clients.index');
    }
}
