<?php

namespace App\Modules\Clients\Backend\Data;

use CfDigital\Delta\Core\Services\CRUDFormGenerator;
use CfDigital\Delta\Core\Services\Data;
use Illuminate\Database\Eloquent\Model;
use CfDigital\Delta\Core\Rules\ImageRule;

class ClientData extends Data
{
    public function __construct(
        public array $title,
        public array|null $logo,
        public array $description,
        public array $show_in_main,
        public \CfDigital\Delta\Core\Enums\PublishStatus|null $status,
    )
    {
    }

    public static function rules(): array
    {
        return [
            "title.".default_locale() => 'required',
            "logo" => ['nullable', new ImageRule()],
            "description.".default_locale() => 'required',
            "show_in_main.".default_locale() => 'nullable',
            'status' => 'required',
        ];
    }

    public static function fieldsForCreation(Model $model): array
    {
        return CRUDFormGenerator::new($model, 'Default')
            ->addTextField('title', ["required" => true])
            ->addTextareaField('description', ["required" => true])
            ->addCheckboxField('show_in_main')
            ->addStatusField()
            ->addMediaFields()
            ->get();
    }
}
