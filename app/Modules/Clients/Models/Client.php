<?php

namespace App\Modules\Clients\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Modules\Clients\Database\Factories\ClientFactory;
use Spatie\LaravelData\WithData;
use Spatie\Translatable\HasTranslations;
use Kalnoy\Nestedset\NodeTrait;
use CfDigital\Delta\Core\Services\Contracts\MediaContract;
use CfDigital\Delta\Core\Services\Traits\HasMedia;
use CfDigital\Delta\Core\Services\Traits\HasPublishStatus;

class Client extends Model implements MediaContract
{
    use HasTranslations;
    use NodeTrait;
    use HasMedia;
    use HasPublishStatus;
    use HasFactory;
    use WithData;

    protected $table = 'clients';
    public $fillable = ['title', 'logo', 'description', 'show_in_main', 'status'];

    public $mediaFields = [ 'logo' => [
            'quantity' => 'single',
            'accept' => 'image/*',
            'sizes' => [
                'page' => ['width' => 600]
            ]
        ]];
    public $translatable = ['title', 'description', 'show_in_main'];

    protected static function newFactory()
    {
        return new ClientFactory();
    }
}
