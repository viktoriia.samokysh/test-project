<?php

namespace App\Modules\Experts\Http\Resources;

use App\Modules\Services\Http\Resources\ServicesResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ExpertInnerResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'full_name' => $this->title,
            'post' => $this->post,
            'photo' => $this->preparedMedia('list')['photo'] ?? null,
            'rating' => $this->rating,
            'email' => $this->email,
            'facebook_link' => $this->facebook_link,
            'quote' => $this->quote,
            'services' => ServicesResource::collection($this->services),
            'experience' => $this->experience,
            'membership' => $this->membership,
            'interests' => $this->interests,
        ];
    }
}
