<?php

namespace App\Modules\Experts\Http\Resources;

use App\Modules\Services\Http\Resources\ServicesResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ExpertsResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'full_name' => $this->title,
            'post' => $this->post,
            'photo' => $this->preparedMedia('list')['photo'] ?? null,
            'email' => $this->email,
        ];
    }
}
