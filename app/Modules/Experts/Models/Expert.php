<?php

namespace App\Modules\Experts\Models;

use App\Modules\Services\Models\Service;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Modules\Experts\Database\Factories\ExpertFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Spatie\LaravelData\WithData;
use Spatie\Translatable\HasTranslations;
use Kalnoy\Nestedset\NodeTrait;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use CfDigital\Delta\Core\Services\Contracts\MediaContract;
use CfDigital\Delta\Core\Services\Traits\HasMedia;
use CfDigital\Delta\Seo\Services\Contracts\GoesToStructureContract;
use CfDigital\Delta\Seo\Services\Traits\GoesToStructure;
use Illuminate\Support\Str;
use CfDigital\Delta\Core\Services\Traits\HasPublishStatus;

class Expert extends Model implements GoesToStructureContract, MediaContract
{
    use HasTranslations;
    use NodeTrait;
    use HasSlug;
    use HasMedia;
    use GoesToStructure;
    use HasPublishStatus;
    use HasFactory;
    use WithData;

    protected $table = 'experts';
    public $fillable = ['title', 'post', 'photo', 'rating', 'email', 'facebook_link', 'quote', 'status', 'experience', 'membership', 'interests'];

    public $mediaFields = [ 'photo' => [
            'quantity' => 'single',
            'accept' => 'image/*',
            'sizes' => [
                'page' => ['width' => 600]
            ]
        ]];

    public $translatable = ['title', 'post', 'quote', 'experience', 'membership', 'interests'];
    protected string $structure_template = "expertsInner";

    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('slug')
            ->slugsShouldBeNoLongerThan(50)
            ->doNotGenerateSlugsOnUpdate()
            ->preventOverwrite();
    }

    public function services(): BelongsToMany
    {
        return $this->belongsToMany(Service::class);
    }

    public function getRouteKeyName(): string
    {
        return 'slug';
    }

    public function generateUrl(): string
    {
        return Str::start('experts/' . $this->slug, '/');
    }

    protected static function newFactory()
    {
        return new ExpertFactory();
    }
}
