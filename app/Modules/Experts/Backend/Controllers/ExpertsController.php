<?php

namespace App\Modules\Experts\Backend\Controllers;

use App\Modules\Experts\Backend\Data\ExpertData;
use App\Modules\Experts\Backend\Tables\ExpertsTable;
use App\Modules\Experts\Models\Expert;
use CfDigital\Delta\Core\Actions\CreateAction;
use CfDigital\Delta\Core\Actions\UpdateAction;
use CfDigital\Delta\Core\Http\Controllers\Api\CRUDController;

class ExpertsController extends CRUDController
{
    protected string $table_class = ExpertsTable::class;
    protected string $model_class = Expert::class;
    protected string $data_class = ExpertData::class;

    public function store(ExpertData $data)
    {
        $expert = (new CreateAction())->handle($data->toArray(), new Expert);
        $expert->services()->sync($data->toArray()['service_id'] ?? []);

        return response()->success();
    }

    public function update(ExpertData $data, $expert)
    {
        $expert = Expert::where('id', $expert)->firstOrFail();

        (new UpdateAction())->handle($data->toArray(), $expert);
        $expert->services()->sync($data->toArray()['service_id'] ?? []);

        return response()->success();
    }
}
