<?php

namespace App\Modules\Experts\Backend\Tables;

use App\Modules\Experts\Models\Expert;
use CfDigital\Delta\Core\Services\Table;
use Illuminate\Http\Request;

class ExpertsTable extends Table
{
    protected const COLUMNS = [
        'full_name' => [
            'type' => 'text'
        ],
        'post' => [
            'type' => 'text'
        ],
        'email' => [
            'type' => 'text'
        ],
        'services' => [
            'type' => 'text'
        ],
        '__actions' => [
            'edit', 'delete', 'show'
        ]
    ];

    protected bool $orderable = true;

    protected string $model = Expert::class;

    protected function map($item): array
    {
        return [
            'id' => $item->id,
            'full_name' => $this->text($item->title),
            'post' => $this->text($item->post),
            'email' => $this->text($item->email),
            'services' => $this->text($item->services->pluck('title')),
            '__show' => $item->generateUrl(),
        ];
    }

    protected function getBasePath(): string
    {
        return noApiRoute('experts.index');
    }
}
