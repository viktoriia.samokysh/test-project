<?php

namespace App\Modules\Experts\Backend\Data;

use App\Modules\Services\Models\Service;
use CfDigital\Delta\Core\Services\CRUDFormGenerator;
use CfDigital\Delta\Core\Services\Data;
use CfDigital\Delta\Core\Services\Fields\TextTemplate;
use Illuminate\Database\Eloquent\Model;
use CfDigital\Delta\Core\Rules\ImageRule;

class ExpertData extends Data
{
    public function __construct(
        public array $title,
        public array $post,
        public array|null $photo,
        public int $rating,
        public string $email,
        public string|null $facebook_link,
        public array $quote,
        public array|null $experience,
        public array|null $membership,
        public array|null $interests,
        public array|null $service_id,
        public \CfDigital\Delta\Core\Enums\PublishStatus|null $status,
    )
    {
    }

    public static function rules(): array
    {
        return [
            "title.".default_locale() => 'required',
            "post.".default_locale() => 'required',
            "photo" => ['nullable', new ImageRule()],
            "rating" => 'required',
            "email" => 'required',
            "facebook_link" => 'nullable',
            "quote.".default_locale() => 'required',
            "experience.".default_locale() => 'nullable',
            "membership.".default_locale() => 'nullable',
            "interests.".default_locale() => 'nullable',
            'status' => 'required',
            'service_id' => 'nullable',
        ];
    }

    public static function fieldsForCreation(Model $model): array
    {
        return CRUDFormGenerator::new($model, 'Main info')
            ->addTextField('title', ["required" => true])
            ->addTextField('post', ["required" => true])
            ->addNumberField('rating', ["required" => true])
            ->addEmailField('email', ["required" => true])
            ->addUrlField('facebook_link')
            ->addTextareaField('quote', ["required" => true])
            ->addStatusField()
            ->addMediaFields()
            ->addSelectField('service_id', [
                'options' => Service::all()
                    ->map(fn ($service) => [
                        'value' => $service->id,
                        'label' => $service->title,
                    ]),
                'multi' => true,
                'value' => $model->services()->pluck('id')->toArray()
            ])
            ->addTab('Additional')
            ->addRepeaterField('experience', [
                'depth' => 1,
                'data' => [
                    'experience' => TextTemplate::prepare(
                        label: trans('delta::fields.experience'),
                        additional: ['required' => false, 'translated' => true]
                    ),
                ]
            ])
            ->addRepeaterField('membership', [
                'depth' => 1,
                'data' => [
                    'membership' => TextTemplate::prepare(
                        label: trans('delta::fields.membership'),
                        additional: ['required' => false]
                    ),
                ]
            ])
            ->addRepeaterField('interests', [
                'depth' => 1,
                'data' => [
                    'interests' => TextTemplate::prepare(
                        label: trans('delta::fields.interests'),
                        additional: ['required' => false]
                    ),
                ]
            ])
            ->get();
    }
}
