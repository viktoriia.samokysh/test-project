<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Area extends Model
{
    use HasTranslations;
    use HasFactory;

    public $translatable = ['title'];

    protected $fillable = [
        'title',
        'system_ref',
    ];

    public function cities()
    {
        return $this->hasMany(City::class, 'area_id', 'id');
    }
}
