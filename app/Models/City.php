<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class City extends Model
{
    use HasTranslations;
    use HasFactory;

    public $incrementing = false;
    protected $keyType = 'string';

    public $translatable = ['title', 'area', 'region', 'settlement_type'];

    protected $fillable = [
        'id',
        'title',
        'area',
        'region',
        'settlement_type',
        'area_id',
        'np_sys_ref',
        'delivery_sys_ref',
        'np_city_ref',
    ];

    public function departments()
    {
        return $this->hasMany(Department::class, 'city_id', 'id');
    }
}
