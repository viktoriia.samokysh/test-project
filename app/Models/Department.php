<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Department extends Model
{
    use HasTranslations;
    use HasFactory;

    public $translatable = ['name', 'address'];

    protected $fillable = [
        'name',
        'address',
        'delivery_type',
        'work_hours',
        'system_ref',
        'system_city_ref',
        'city_id',
    ];
}
