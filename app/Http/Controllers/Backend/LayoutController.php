<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class LayoutController extends Controller
{
    public function __invoke(Request $request)
    {
        $locales = [];
        foreach (config('delta.supportedLocales') as $locale => $arr) {
            $locales[] = [
                ...$arr,
                'is_default' => default_locale() === $locale,
                'locale' => $locale
            ];
        }

        return [
            'menu' => $this->menu($request),
            'locales' => $locales
        ];
    }

    private function menu(Request $request)
    {
        return [
            'users' => [
                'title' => trans('delta::menu.users'),
                'icon' => 'person',
                'items' => [
                    [
                        'title' => trans('delta::menu.users'),
                        'url' => noApiRoute('users.index'),
                    ],
                    [
                        'title' => trans('delta::menu.roles'),
                        'url' => noApiRoute('roles.index'),
                    ],
                ]
            ],
            'pages' => [
                'title' => trans('delta::menu.pages'),
                'url' => noApiRoute('domains.index'),
                'icon' => 'web'
            ],
            'infoblocks' => [
                'title' => trans('delta::menu.infoblocks'),
                'url' => noApiRoute('infoblocks.index'),
                'icon' => 'polyline'
            ],
            'forms' => [
                'title' => trans('delta::menu.forms'),
                'url' => noApiRoute('forms.index'),
                'icon' => 'web'
            ],
            'banners' => [
                'title' => trans('delta::menu.banners'),
                'url' => noApiRoute('banners.index'),
                'icon' => 'web'
            ],
            'reviews' => [
                'title' => trans('delta::menu.reviews'),
                'url' => noApiRoute('reviews.index'),
                'icon' => 'chat'
            ],
            'clients' => [
                'title' => trans('delta::menu.clients'),
                'url' => noApiRoute('clients.index'),
                'icon' => 'person'
            ],
            'tests' => [
                'title' => trans('delta::menu.tests'),
                'url' => noApiRoute('tests.index'),
                'icon' => 'chat'
            ],
            'projects' => [
                'title' => trans('delta::menu.projects'),
                'url' => noApiRoute('projects.index'),
                'icon' => 'web'
            ],
            'services' => [
                'title' => trans('delta::menu.services'),
                'url' => noApiRoute('services.index'),
                'icon' => 'web'
            ],
            'experts' => [
                'title' => trans('delta::menu.experts'),
                'url' => noApiRoute('experts.index'),
                'icon' => 'person'
            ],
            'posts' => [
                'title' => trans('delta::menu.posts'),
                'url' => noApiRoute('posts.index'),
                'icon' => 'web'
            ],
            'seo' => [
                'title' => trans('delta::menu.seo'),
                'icon' => 'search',
                'items' => [
                    [
                        'title' => 'Structures',
                        'url' => noApiRoute('structures.index'),
                    ],
                    [
                        'title' => 'Redirects',
                        'url' => noApiRoute('redirects.index'),
                    ],
                    [
                        'title' => 'Metatags',
                        'url' => noApiRoute('metatags.index'),
                    ],
                    [
                        'title' => 'Robots',
                        'url' => noApiRoute('robots'),
                    ],
                ]
            ],
            'system' => [
                'title' => trans('delta::menu.system'),
                'icon' => 'settings',
                'items' => [
                    [
                        'title' => trans('delta::menu.translations'),
                        'url' => noApiRoute('translations.show'),
                    ],
                    [
                        'title' => trans('delta::menu.menu'),
                        'url' => noApiRoute('menus.index'),
                    ],
                    [
                        'title' => trans('delta::menu.settings'),
                        'url' => noApiRoute('settings.index'),
                    ]
                ]
            ]
        ];
    }
}
