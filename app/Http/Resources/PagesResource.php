<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PagesResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'title' => $this->title,
            'content' => $this->content,
            'description' => $this->description,
//            'slider' => ($this->extra['slider_id'] ?? false) ? (new SlidersEditorElement)->render($this->slider_id)['data']['slider'] : null,
            'extra' => $this->extra[app()->getLocale()] ?? null,
            ...$this->preparedMedia(type: 'page'),
        ];
    }
}
