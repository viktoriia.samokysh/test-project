<?php

namespace App\Console\Commands;

use App\Models\City;
use App\Models\Department;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class AddDeliveryCityRef extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:add-delivery-city-ref';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $baseApiUrl = 'https://www.delivery-auto.com';
        $langUk = 'uk-UA';

        $client = new \GuzzleHttp\Client();
        $cityResponse = $client->request('GET', "$baseApiUrl/api/v4/Public/GetAreasList?culture=$langUk&country=1&fl_all=true");
        $cityResponseBody = json_decode($cityResponse->getBody());
        $cityResponseData = $cityResponseBody->data ?? [];

        $fp = fopen('notFoundCities.csv', 'w');
        fputcsv($fp, ['id', 'name', 'area', 'region']);

        foreach ($cityResponseData as $city) {
            $area = explode(" ", $city->regionName);

            $settlement = City::whereJsonContains('title->uk', $city->name)
                ->where('area', 'like', '%'.$area[0].'%')
                ->where('region', 'like', '%'.$city->districtName.'%')
                ->first();

            if (!$settlement) {
                $settlement = City::whereJsonContains('title->uk', $city->name)
                    ->where('area', 'like', '%'.$city->regionName.'%')
                    ->first();
            }

            if (!$settlement) {
                fputcsv($fp, [$city->id, $city->name, $city->regionName, $city->districtName]);
            }

            $settlement?->update(['delivery_sys_ref' => $city->id]);
        }

        fclose($fp);

        $deparmentsCityRefs = DB::table('departments')
            ->select('system_city_ref')
            ->where('delivery_type', 'delivery')
            ->distinct()
            ->get();

        foreach ($deparmentsCityRefs as $ref) {
            $city = City::where('delivery_sys_ref', $ref->system_city_ref)->first();

            if ($city) {
                Department::where('delivery_type', 'delivery')
                    ->where('system_city_ref', $ref->system_city_ref)
                    ->update(['city_id' => $city->id]);
            }
        }
    }
}
