<?php

namespace App\Console\Commands;

use App\Models\Department;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;

class GetDevileryDepartments extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:get-devilery-departments';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $baseApiUrl = 'https://www.delivery-auto.com';
        $langUk = 'uk-UA';
        $langEn = 'en-US';

        $client = new \GuzzleHttp\Client();
        $warehouseResponse = $client->request('GET', "$baseApiUrl/api/v4/Public/GetWarehousesList?culture=$langUk&country=1");
        $warehouseResponseBody = json_decode($warehouseResponse->getBody());
        $warehouseResponseData = $warehouseResponseBody->data ?? [];

        $warehouseResponseEn = $client->request('GET', "$baseApiUrl/api/v4/Public/GetWarehousesList?culture=$langEn&country=1");
        $warehouseResponseBodyEn = json_decode($warehouseResponseEn->getBody());
        $warehouseResponseDataEn = $warehouseResponseBodyEn->data ?? [];

        Department::where('delivery_type', 'delivery')->delete();

        foreach ($warehouseResponseData as $warehouse) {
            Department::create([
                'name' => ['uk' => $warehouse->name],
                'address' => ['uk' => $warehouse->address],
                'delivery_type' => 'delivery',
                'system_ref' => $warehouse->id,
                'system_city_ref' => $warehouse->CityId,
            ]);
        }

        App::setLocale('uk');

        foreach ($warehouseResponseDataEn as $warehouse) {
            $exist = Department::where('system_ref', $warehouse->id)
                ->where('delivery_type', 'delivery')
                ->first();

            if ($exist) {
                $exist->update([
                    'name' => array_merge(['uk' => $exist->name], ['en' => $warehouse->name]),
                    'address' => array_merge(['uk' => $exist->address], ['en' => $warehouse->address]),
                ]);
            }
        }
    }
}
