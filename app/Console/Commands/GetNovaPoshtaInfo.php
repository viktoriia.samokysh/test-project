<?php

namespace App\Console\Commands;

use App\Models\Area;
use App\Models\City;
use App\Models\Department;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;

class GetNovaPoshtaInfo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:get-nova-poshta-info';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get np cities and areas';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $client = new \GuzzleHttp\Client();
        $baseApiUrl = 'https://api.novaposhta.ua/v2.0/json/';

        $citiesResponse = $client->request('POST', $baseApiUrl, [
            'json' => [
                'apiKey' => '',
                'modelName' => 'Address',
                "calledMethod" => "getSettlements",
                "methodProperties" => [
                    'Page' => 1,
                    'Limit' => 1,
                ]
            ]
        ]);

        $citiesResponseBody = json_decode($citiesResponse->getBody());
        $totalCount = $citiesResponseBody?->info?->totalCount;

        $limit = 150;
        $pages = ceil($totalCount / $limit);

        City::truncate();

        for ($i = 1; $i <= $pages; $i++) {
            $citiesResponse = $client->request('POST', $baseApiUrl, [
                'json' => [
                    'apiKey' => '',
                    'modelName' => 'Address',
                    "calledMethod" => "getSettlements",
                    "methodProperties" => [
                        'Page' => $i,
                        'Limit' => $limit,
                    ]
                ]
            ]);

            $citiesResponseBody = json_decode($citiesResponse->getBody());
            $citiesResponseData = $citiesResponseBody->data ?? [];

            $settlementTypesEn = [
                'місто' => 'city',
                'селище міського типу' => 'urban village',
                'село' => 'village',
                'селище' => 'village',
            ];

            foreach ($citiesResponseData as $city) {
                City::create([
                    'id' => $city->Ref,
                    'title' => ['uk' => $city->Description, 'en' => $city->DescriptionTranslit],
                    'area' => ['uk' => $city->AreaDescription, 'en' => $city->AreaDescriptionTranslit],
                    'region' => ['uk' => $city->RegionsDescription, 'en' => $city->RegionsDescriptionTranslit],
                    'settlement_type' => ['uk' => $city->SettlementTypeDescription, 'en' => $settlementTypesEn[$city->SettlementTypeDescription] ?? null],
                    'np_sys_ref' => $city->Ref,
                ]);
            }
        }

        $areas = City::select('area')->distinct()->get();

        Area::truncate();

        foreach ($areas as $area) {
            App::setLocale('uk');
            $nameUk = $area->area;

            App::setLocale('en');
            $nameEn = $area->area;

            $item = Area::create([
                'title' => ['uk' => $nameUk, 'en' => $nameEn],
            ]);

            City::whereJsonContains('area', ['en' => $nameEn])->update([
                'area_id' => $item->id
            ]);
        }
    }
}
