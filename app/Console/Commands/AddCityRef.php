<?php

namespace App\Console\Commands;

use App\Models\City;
use Illuminate\Console\Command;

class AddCityRef extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:add-city-ref';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $client = new \GuzzleHttp\Client();
        $baseApiUrl = 'https://api.novaposhta.ua/v2.0/json/';

        $citiesResponse = $client->request('POST', $baseApiUrl, [
            'json' => [
                'apiKey' => '',
                'modelName' => 'Address',
                "calledMethod" => "getCities",
                "methodProperties" => [
                    'Page' => 1,
                    'Limit' => 1,
                ]
            ]
        ]);

        $citiesResponseBody = json_decode($citiesResponse->getBody());
        $totalCount = $citiesResponseBody?->info?->totalCount;

        $limit = 150;
        $pages = ceil($totalCount / $limit);

        for ($i = 1; $i <= $pages; $i++) {
            $citiesResponse = $client->request('POST', $baseApiUrl, [
                'json' => [
                    'apiKey' => '',
                    'modelName' => 'Address',
                    "calledMethod" => "getCities",
                    "methodProperties" => [
                        'Page' => $i,
                        'Limit' => $limit,
                    ]
                ]
            ]);

            $citiesResponseBody = json_decode($citiesResponse->getBody());
            $citiesResponseData = $citiesResponseBody->data ?? [];

            foreach ($citiesResponseData as $city) {
                $shortDescription = trim(explode('(', $city->Description)[0]);

                preg_match('/([А-ЩЬЮЯҐЄІЇ][а-щьюяґєії\']*) р-н/u', $city->Description, $matches);
                $region = $matches[1] ?? null;

                $baseRequest = City::whereJsonContains('title->uk', $shortDescription)
                    ->where('area', 'like', '%'.$city->AreaDescription.'%');

                if ($region) {
                    $baseRequest->where('region', 'like', '%'.$region.'%');
                }

                $settlement = $baseRequest->first();

                $settlement?->update(['np_city_ref' => $city->Ref]);
            }
        }
    }
}
