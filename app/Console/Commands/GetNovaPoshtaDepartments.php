<?php

namespace App\Console\Commands;

use App\Models\City;
use App\Models\Department;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;

class GetNovaPoshtaDepartments extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:get-nova-poshta-departments';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $client = new \GuzzleHttp\Client();
        $baseApiUrl = 'https://api.novaposhta.ua/v2.0/json/';

        Department::where('delivery_type', 'np')->delete();

        $departmentResponse = $client->request('POST', $baseApiUrl, [
            'json' => [
                'apiKey' => '',
                'modelName' => 'Address',
                "calledMethod" => "getWarehouses",
                "methodProperties" => [
                    'Page' => 1,
                    'Limit' => 1,
                ]
            ]
        ]);

        $departmentResponseBody = json_decode($departmentResponse->getBody());
        $departmentTotal = $departmentResponseBody?->info?->totalCount;

        $limit = 500;
        $pages = ceil($departmentTotal / $limit);

        for ($i = 1; $i <= $pages; $i++) {
            $departmentResponse = $client->request('POST', $baseApiUrl, [
                'json' => [
                    'apiKey' => '',
                    'modelName' => 'Address',
                    "calledMethod" => "getWarehouses",
                    "methodProperties" => [
                        'Page' => $i,
                        'Limit' => $limit,
                    ]
                ]
            ]);

            $departmentResponseBody = json_decode($departmentResponse->getBody());
            $departmentResponseData = $departmentResponseBody->data ?? [];

            foreach ($departmentResponseData as $department) {
                Department::create([
                    'name' => ['uk' => $department->Description, 'en' => null],
                    'address' => ['uk' => $department->ShortAddress, 'en' => null],
                    'delivery_type' => 'np',
                    'system_ref' => $department->Ref,
                    'system_city_ref' => $department->SettlementRef,
                    'city_id' => $department->SettlementRef,
                ]);
            }
        }
    }
}
