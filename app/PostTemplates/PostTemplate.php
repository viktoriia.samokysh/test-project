<?php

namespace App\PostTemplates;

use Spatie\ModelStates\Attributes\DefaultState;
use App\Modules\Posts\Models\Post;
use Spatie\ModelStates\State;

#[
    DefaultState(NewsPostTemplate::class),
]
abstract class PostTemplate extends State
{
    public static function getFields(Post $post): array
    {
        return [];
    }
}
