<?php

namespace App\PostTemplates;

use App\Modules\Posts\Models\Post;
use CfDigital\Delta\Core\Services\CRUDFormGenerator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class NewsPostTemplate extends PostTemplate
{
    public static string $name = 'news';

    public static function getFields(Post $post): array
    {
        return CRUDFormGenerator::new($post)
            ->tabless()
            ->addCheckboxField('in_main', ['value' => $post->extra['in_main'] ?? null])
            ->addTextField('description', ['value' => $post->extra['description'] ?? null])
            ->get();
    }


    public static function withInData(Request $request): array
    {
        return [
            'in_main' => $request->input('in_main'),
            'description' => $request->input('description'),
        ];
    }

    public static function rules(): array
    {
        return [
            "in_main" => 'nullable|boolean',
            "description.".default_locale() => 'nullable',
        ];
    }
}
