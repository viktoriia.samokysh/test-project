<?php

namespace App\PostTemplates;

use App\Modules\Experts\Models\Expert;
use App\Modules\Posts\Models\Post;
use CfDigital\Delta\Core\Services\CRUDFormGenerator;
use CfDigital\Delta\Core\Services\Fields\TextTemplate;
use CfDigital\Delta\Forms\Models\Form;
use Illuminate\Http\Request;

class PublicationsPostTemplate extends PostTemplate
{
    public static string $name = 'publications';

    public static function getFields(Post $post): array
    {
        $expert_id = isset($post->extra['expert_id']) ? (int) $post->extra['expert_id'] : null;

        return CRUDFormGenerator::new($post)
            ->tabless()
            ->addRepeaterField('tags', [
                'value' => $post->extra['tags'] ?? null,
                'required' => false,
                'depth' => 1,
                'data' => [
                    'title' => TextTemplate::prepareTranslated(
                        label: trans('delta::fields.tags'),
                        additional: ['required' => true]
                    ),
                ]
            ])
            ->addSelectField('expert_id', [
                'value' => $expert_id,
                'required' => true,
                'options' => Expert::all()
                    ->map(fn ($expert) => [
                        'value' => $expert->id,
                        'label' => $expert->title
                    ])
            ])
            ->get();
    }

    public static function withInData(Request $request): array
    {
        return [
            'tags' => $request->input('tags'),
            'expert_id' => $request->input('expert_id'),
        ];
    }

    public static function rules(): array
    {
        return [
            "expert_id" => 'required|integer',
            "tags.".default_locale() => 'nullable',
        ];
    }
}
