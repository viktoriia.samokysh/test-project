<?php

namespace App\BannerTemplates;

use App\BannerTemplates\BannerTemplate;
use App\Modules\Banners\Models\BannerImage;
use CfDigital\Delta\Core\Services\CRUDFormGenerator;

class DefaultBannerImageTemplate extends BannerImageTemplate
{
    public static string $name = 'default';

    public static function getFields(BannerImage $image): array
    {
        return CRUDFormGenerator::new($image)
            ->tabless()
            ->get();
    }
}
