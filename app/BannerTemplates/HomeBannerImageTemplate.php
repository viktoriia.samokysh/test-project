<?php

namespace App\BannerTemplates;

use App\Modules\Banners\Models\BannerImage;
use CfDigital\Delta\Core\Services\CRUDFormGenerator;
use Illuminate\Http\Request;

class HomeBannerImageTemplate extends BannerImageTemplate
{
    public static string $name = 'home';

    public static function getFields(BannerImage $image): array
    {
        return CRUDFormGenerator::new($image)
            ->tabless()
            ->addTextField('title', ['value' => $image->extra['title'] ?? null])
            ->addTextField('button_text', ['value' => $image->extra['button_text'] ?? null])
            ->addTextField('description', ['value' => $image->extra['description'] ?? null])
            ->get();
    }

    public static function withInData(Request $request): array
    {
        return [
            'title' => $request->input('title'),
            'button_text' => $request->input('button_text'),
            'description' => $request->input('description'),
        ];
    }

    public static function rules(): array
    {
        return [
            "title" => 'nullable|string',
            "button_text" => 'nullable|string',
            "description" => 'nullable',
        ];
    }
}
