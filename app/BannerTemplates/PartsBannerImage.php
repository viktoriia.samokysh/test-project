<?php

namespace App\BannerTemplates;

use App\Modules\Banners\Models\BannerImage;
use CfDigital\Delta\Core\Services\CRUDFormGenerator;
use Illuminate\Http\Request;

class PartsBannerImage extends BannerImageTemplate
{
    public static string $name = 'parts';

    public static function getFields(BannerImage $image): array
    {
        return CRUDFormGenerator::new($image)
            ->tabless()
            ->addTextField('title', ['value' => $image->extra[default_locale()]['title'] ?? null])
            ->get();
    }

    public static function withInData(Request $request): array
    {
        return [
            'title' => $request->input('title'),
        ];
    }

    public static function rules(): array
    {
        return [
            "title" => 'nullable|string',
        ];
    }
}
