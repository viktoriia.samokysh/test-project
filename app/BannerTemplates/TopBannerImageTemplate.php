<?php

namespace App\BannerTemplates;

use App\Modules\Banners\Models\BannerImage;
use CfDigital\Delta\Core\Rules\ImageRule;
use CfDigital\Delta\Core\Services\CRUDFormGenerator;
use Illuminate\Http\Request;

class TopBannerImageTemplate extends BannerImageTemplate
{
    public static string $name = 'top';

    public static function getFields(BannerImage $image): array
    {
        return CRUDFormGenerator::new($image)
            ->tabless()
            ->addColorField('color', ['value' => $image->extra['color'] ?? null])
            ->addImageField('pattern')
            ->get();
    }

    public function getMedia()
    {
        return  [
            'pattern' => [
                'quantity' => 'single',
                'accept' => 'image/*',
                'sizes' => [
                    'list' => ['width' => 600]
                ]
            ]
        ];
    }

    public static function withInData(Request $request): array
    {
        return [
            'color' => $request->input('color'),
            'pattern' => getMediaFromRequest($request, 'pattern')
        ];
    }

    public static function rules(): array
    {
        return [
            "color" => 'nullable',
            "pattern" => ['nullable', new ImageRule()],
        ];
    }
}
