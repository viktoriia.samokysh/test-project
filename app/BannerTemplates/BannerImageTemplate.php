<?php

namespace App\BannerTemplates;

use App\Modules\Banners\Models\BannerImage;
use Spatie\ModelStates\Attributes\DefaultState;
use Spatie\ModelStates\State;

#[
    DefaultState(DefaultBannerImageTemplate::class),
]
abstract class BannerImageTemplate extends State
{
    public static function getFields(BannerImage $image): array
    {
        return [];
    }
}
