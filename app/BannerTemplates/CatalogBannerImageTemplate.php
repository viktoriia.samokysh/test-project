<?php

namespace App\BannerTemplates;

use App\Modules\Banners\Models\BannerImage;
use CfDigital\Delta\Core\Services\CRUDFormGenerator;
use Illuminate\Http\Request;

class CatalogBannerImageTemplate extends BannerImageTemplate
{
    public static string $name = 'catalog';

    public static function getFields(BannerImage $image): array
    {
        return CRUDFormGenerator::new($image)
            ->tabless()
            ->addTextField('button_text', ['value' => $image->extra[default_locale()]['button_text'] ?? null])
            ->addColorField('color', ['value' => $image->extra[default_locale()]['color'] ?? null, 'default' => '000000'])
            ->get();
    }

    public static function withInData(Request $request): array
    {
        return [
            'button_text' => $request->input('button_text'),
            'color' => $request->input('color'),
        ];
    }

    public static function rules(): array
    {
        return [
            "button_text" => 'nullable|string',
            "color" => 'nullable',
        ];
    }
}
