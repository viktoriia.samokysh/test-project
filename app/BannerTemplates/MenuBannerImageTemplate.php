<?php

namespace App\BannerTemplates;

use App\Modules\Banners\Models\BannerImage;
use CfDigital\Delta\Core\Rules\ImageRule;
use CfDigital\Delta\Core\Services\CRUDFormGenerator;
use Illuminate\Http\Request;

class MenuBannerImageTemplate extends BannerImageTemplate
{
    public static string $name = 'menu';

    public static function getFields(BannerImage $image): array
    {
        return CRUDFormGenerator::new($image)
            ->tabless()
            ->addTextField('title', ['value' => $image->extra[default_locale()]['title'] ?? null])
            ->addTextField('button_text', ['value' => $image->extra[default_locale()]['button_text'] ?? null])
            ->addColorField('color', ['value' => $image->extra[default_locale()]['color'] ?? null, 'default' => 'ffffff'])
            ->get();
    }

    public static function withInData(Request $request): array
    {
        return [
            'title' => $request->input('title'),
            'button_text' => $request->input('button_text'),
            'color' => $request->input('color'),
        ];
    }

    public static function rules(): array
    {
        return [
            "title" => 'nullable|string',
            "button_text" => 'nullable|string',
            "color" => 'nullable',
        ];
    }
}
