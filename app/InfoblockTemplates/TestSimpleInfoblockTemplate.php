<?php

namespace App\InfoblockTemplates;

use CfDigital\Delta\Core\Services\Fields\RepeaterTemplate;
use CfDigital\Delta\Core\Services\Fields\TextTemplate;
use CfDigital\Delta\Infoblocks\Models\Infoblock;

class TestSimpleInfoblockTemplate extends InfoblockTemplate
{
    public static string $name = 'test-simple';

    public static function getFields(Infoblock $infoblock): array
    {
        return [
            'blocks' => RepeaterTemplate::prepare(
                label: trans('delta::fields.blocks'),
                value: $infoblock->data['blocks'] ?? null,
                additional: [
                    'required' => true,
                    'data' => [
                        'description' => TextTemplate::prepareTranslated(
                            label: trans('delta::fields.description'),
                            value: $infoblock->data['blocks']['description'] ?? null,
                            additional: ['required' => true]
                        ),
                    ]
                ]
            )
        ];
    }

    public function rules(): array
    {
        return [
            'blocks.*.description.' . default_locale() => 'max:250|string|required',
        ];
    }
}
