<?php

namespace App\InfoblockTemplates;

use CfDigital\Delta\Core\Rules\ImageRule;
use CfDigital\Delta\Core\Services\Fields\FileTemplate;
use CfDigital\Delta\Core\Services\Fields\RepeaterTemplate;
use CfDigital\Delta\Core\Services\Fields\TextTemplate;
use CfDigital\Delta\Infoblocks\Models\Infoblock;

class TestExtraInfoblockTemplate extends InfoblockTemplate
{
    public static string $name = 'test-extra';

    public static function getFields(Infoblock $infoblock): array
    {
        return [
            'blocks' => RepeaterTemplate::prepare(
                label: trans('delta::fields.blocks'),
                value: $infoblock->data['blocks'] ?? null,
                additional: [
                    'required' => true,
                    'data' => [
                        'email' => TextTemplate::prepare(
                            label: trans('delta::fields.email'),
                            value: $infoblock->data['blocks']['email'] ?? null,
                            additional: ['type' => 'email']
                        ),
                        'phone' => TextTemplate::prepare(
                            label: trans('delta::fields.phone'),
                            value: $infoblock->data['blocks']['phone'] ?? null,
                        ),
                        'color' => TextTemplate::prepare(
                            label: trans('delta::fields.color'),
                            value: $infoblock->data['blocks']['color'] ?? null,
                            additional: ['type' => 'color']
                        ),
                        'blocks' => FileTemplate::prepare(
                            label: trans('delta::fields.image'),
                            value: $infoblock->data['blocks']['image'] ?? null,
                        ),
                    ]
                ]
            )
        ];
    }

    public function rules(): array
    {
        return [
            'blocks.*.email' => 'string|email|nullable',
            'blocks.*.phone' => 'string|nullable',
            'blocks.*.color' => 'string|nullable',
            'blocks.*.image' => ['nullable', new ImageRule()],
        ];
    }
}
