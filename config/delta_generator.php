<?php

return [
    'Tests' => [
        'Test' => [
            'fields' => [
                'text_translated' => [
                    'translated' => true,
                    'type' => 'text',
                    'required' => false,
                ],
                'text' => [
                    'translated' => false,
                    'type' => 'text',
                    'required' => false,
                ],
                'media' => [
                    'translated' => false,
                    'type' => 'media',
                    'required' => false,
                ],
                'textarea_translated' => [
                    'translated' => true,
                    'type' => 'textarea',
                    'required' => false,
                ],
                'textarea' => [
                    'translated' => false,
                    'type' => 'textarea',
                    'required' => false,
                ],
                'boolean' => [
                    'translated' => false,
                    'type' => 'boolean',
                    'required' => false,
                ],
                'number' => [
                    'translated' => false,
                    'type' => 'number',
                    'required' => false,
                ],
                'date' => [
                    'translated' => false,
                    'type' => 'date',
                    'required' => false,
                ],
                'datetime' => [
                    'translated' => false,
                    'type' => 'datetime',
                    'required' => false,
                ],
                'editor_translated' => [
                    'translated' => false,
                    'type' => 'editor',
                    'required' => false,
                ],
                'editor' => [
                    'translated' => false,
                    'type' => 'editor',
                    'required' => false,
                ],
            ],
            'has_slug' => false,
            'has_translations' => true,
            'has_media' => true,
            'goes_to_structure' => false,
            'has_publish_status' => true,
            'has_order' => true,
            'is_nested' => false,
        ]
    ],
];
