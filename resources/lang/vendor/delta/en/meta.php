<?php

return [
    'seo' => [
        'title' => 'Title',
        'h1_title' => 'H1 title (or title if empty)',
    ],
    'post' => [
        'title' => 'Post title',
        'description' => 'Post description',
        'content' => 'Post content',
        'date_from' => 'Post date from in format "d.m.Y"'
    ],
    'posts' => [
        'count' => 'Posts count'
    ],
    'project' => [
        'title' => 'Project title',
        'content' => 'Project content',
        'area' => 'Project area',
        'quote' => 'Project quote',
        'left_text' => 'Project left text',
        'right_text' => 'Project right text',
        'type' => 'Project type',
        'budget' => 'Project budget',
        'timespan' => 'Project timespan',
        'location' => 'Project location',
        'readiness' => 'Project readiness',
        'short_title' => 'Project short title',
    ],
    'projects' => [
        'count' => 'Projects count'
    ],
];
