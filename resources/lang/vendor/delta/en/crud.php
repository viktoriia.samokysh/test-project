<?php

return [
    'projects' => [
        'index' => 'Projects',
        'create' => 'Create project',
    ],
    'technologies' => [
        'index' => 'Technologies',
        'create' => 'Create technology',
    ],
    'forms' => [
        'index' => 'Forms',
        'create' => 'Create form',
        'fields' => [
            'index' => 'Fields',
            'create' => 'Create field',
        ],
        'responses' => [
            'index' => 'Responses',
        ]
    ],
    'infoblocks' => [
        'index' => 'Infoblocks',
        'create' => 'Create infoblock',
    ],
    'sliders' => [
        'index' => 'Sliders',
        'create' => 'Create slider',
        'slides' => [
            'index' => 'Slides',
            'create' => 'Create slide',
        ]
    ],
    'users' => [
        'index' => 'Users',
        'create' => 'Create user',
    ],
    'roles' => [
        'index' => 'Roles',
        'create' => 'Create role',
    ],
    'posts' => [
        'index' => 'Posts',
        'create' => 'Create post',
    ],
    'structures' => [
        'index' => 'SEO structures',
    ],
    'metatags' => [
        'index' => 'SEO Metatags',
        'create' => 'Create SEO metatag',
    ],
    'redirects' => [
        'index' => 'Redirects',
        'create' => 'Create redirect',
    ],
    'domains' => [
        'index' => 'Domains',
        'pages' => [
            'index' => 'Pages',
            'create' => 'Create page',
        ]
    ],
    'hints' => [
        'individual_tags' => 'If checked, the tags will be taken from the current page, otherwise from the META template',
        'force_index' => 'If checked, the page and all it`s children will be indexed, otherwise "To index" will be used',
        'force_follow' => 'If checked, the page and all it`s children will be followed, otherwise "To follow" will be used',
    ],
    'system' => [
        'edit' => 'Edit',
        'create' => 'Create',
    ],
    'menus' => [
        'index' => 'Menu',
    ],
    'main' => [
        'index' => 'Main'
    ],
    'translations' => [
        'index' => 'Translations'
    ],
    'reviews' => [
        'index' => 'Reviews',
        'create' => 'Create review',
    ],
    'clients' => [
        'index' => 'Clients',
        'create' => 'Create client',
    ],
    'services' => [
        'index' => 'Services',
        'create' => 'Create service',
    ],
    'experts' => [
        'index' => 'Experts',
        'create' => 'Create expert',
    ],
];
