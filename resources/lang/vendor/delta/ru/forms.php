<?php

return [
    'field_templates' => [
        'string' => 'Строка',
        'hidden' => 'Спрятанное',
        'text' => 'Текст',
        'boolean' => 'Булевое значение',
        'select' => 'Выпадающий список',
        'email' => 'E-mail',
        'phone' => 'Телефон',
        'file' => 'Файл',
        'date' => 'Дата',
        'separator' => 'Разделитель',
        'multi_select' => 'Множественный выпадающий список',
        'integer' => 'Число',
    ],
    'mailable' => [
        'auto mailable' => [
            'subject' => 'New form submission from :form on :website',
        ],
    ],
];