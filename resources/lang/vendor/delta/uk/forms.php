<?php

return [
    'field_templates' => [
        'string' => 'Текстовий рядок',
        'hidden' => 'Приховане поле',
        'text' => 'Текст',
        'boolean' => 'Логічне поле',
        'select' => 'Випадаючий список',
        'email' => 'E-mail',
        'phone' => 'Телефон',
        'file' => 'Файл',
        'date' => 'Дата',
        'separator' => 'Роздільник',
        'multi_select' => 'Множинний вибір',
        'integer' => 'Число',
    ],
    'mailable' => [
        'auto mailable' => [
            'subject' => 'New form submission from :form on :website',
        ],
    ],
];
