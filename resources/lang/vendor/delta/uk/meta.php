<?php

return [
    'seo' => [
        'title' => 'Заголовок',
        'h1_title' => 'H1 (або Заголовок, якщо залишити пустим)',
    ],
    'post' => [
        'title' => 'Post title',
        'description' => 'Post description',
        'content' => 'Post content',
        'date_from' => 'Post date from in format "d.m.Y"'
    ]
];
