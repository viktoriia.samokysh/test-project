<?php

return [
    'projects' => [
        'index' => 'Проекти',
        'create' => 'Створити проект',
    ],
    'technologies' => [
        'index' => 'Технології',
        'create' => 'Створити технології',
    ],
    'forms' => [
        'index' => 'Форми',
        'create' => 'Створити форму',
        'fields' => [
            'index' => 'Поля форми',
            'create' => 'Створити',
        ],
        'responses' => [
            'index' => 'Заявки',
        ]
    ],
    'infoblocks' => [
        'index' => 'Інфоблоки',
        'create' => 'Створити інфоблок',
    ],
    'sliders' => [
        'index' => 'Слайдер',
        'create' => 'Створити слайдер',
        'slides' => [
            'index' => 'Слайд',
            'create' => 'Створити слайд',
        ]
    ],
    'users' => [
        'index' => 'Користувачі',
        'create' => 'Створити користувача',
    ],
    'roles' => [
        'index' => 'Ролі',
        'create' => 'Створити роль',
    ],
    'posts' => [
        'index' => 'Статті',
        'create' => 'Створити статтю',
    ],
    'structures' => [
        'index' => 'SEO структура',
    ],
    'metatags' => [
        'index' => 'SEO Метатеги',
        'create' => 'Створити SEO метатег',
    ],
    'redirects' => [
        'index' => 'Редіректи',
        'create' => 'Створити редіректи',
    ],
    'domains' => [
        'index' => 'Домени',
        'pages' => [
            'index' => 'Сторінка',
            'create' => 'Створити сторінку',
        ]
    ],
    'hints' => [
        'individual_tags' => 'If checked, the tags will be taken from the current page, otherwise from the META template',
        'force_index' => 'If checked, the page and all it`s children will be indexed, otherwise "To index" will be used',
        'force_follow' => 'If checked, the page and all it`s children will be followed, otherwise "To follow" will be used',
    ],
    'system' => [
        'edit' => 'Редагувати',
        'create' => 'Створити',
    ],
    'settings' => [
        'index' => 'Налаштування'
    ],
    'robots' => [
        'index' => 'Редагувати файл Robots.txt'
    ],
    'reviews' => [
        'index' => 'Відгук',
        'create' => 'Створити відгук',
    ],
    'clients' => [
        'index' => 'Клієнти',
        'create' => 'Створити клієнта',
    ],
    'services' => [
        'index' => 'Послуги',
        'create' => 'Створити послугу',
    ],
    'experts' => [
        'index' => 'Експерти',
        'create' => 'Створити експерта',
    ],
];
