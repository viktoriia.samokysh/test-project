<?php

use App\Http\Controllers\Api\LayoutController;
use App\Modules\Projects\Http\Controllers\Api\ProjectController;
use CfDigital\Delta\Core\Http\Controllers\HomePageController;
use CfDigital\Delta\Core\Http\Controllers\PageController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
 *
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::prefix(config('delta.backend_prefix'))
    ->middleware(['web', 'api'])
    ->group(function () {
        Route::get('options/layout', App\Http\Controllers\Backend\LayoutController::class);
        Route::resource('reviews', 'App\Modules\Reviews\Backend\Controllers\ReviewsController')->middleware('resource_can:review');
        Route::resource('clients', 'App\Modules\Clients\Backend\Controllers\ClientsController')->middleware('resource_can:client');
        Route::resource('tests', 'App\Modules\Tests\Backend\Controllers\TestsController')->middleware('resource_can:test');
        Route::resource('projects', 'App\Modules\Projects\Backend\Controllers\ProjectsController')->middleware('resource_can:project');
        Route::resource('services', 'App\Modules\Services\Backend\Controllers\ServicesController')->middleware('resource_can:service');
        Route::resource('experts', 'App\Modules\Experts\Backend\Controllers\ExpertsController')->middleware('resource_can:expert');
        Route::resource('posts', 'App\Modules\Posts\Backend\Controllers\PostsController')->middleware('resource_can:post');
        Route::resource('banners', 'App\Modules\Banners\Backend\Controllers\BannersController')->middleware('resource_can:banner');
        Route::resource('banners.images', 'App\Modules\Banners\Backend\Controllers\BannerImagesController')->middleware('resource_can:banner_images');
    });

Route::middleware(['api', 'localize'])
    ->prefix('api')
    ->group(function () {
        Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
            return $request->user();
        });

        Route::get('/', HomePageController::class);
        Route::get('options/layout', LayoutController::class);
        Route::get('/projects', ProjectController::class)->name('projects');
        Route::get('{slug}', PageController::class)->where(['slug' => '.*']);
    });
